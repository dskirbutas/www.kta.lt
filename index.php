<!DOCTYPE html>
<html>
<head>
            <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Klaipėdos tinklinio asociacija</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    
</head>
<body>
<?php  include 'header.php'; ?> <!-- =====================header====================== -->

	<section class="second">
        <wrapper>
    <div id="slider">

        <ul>
            
            <li id="naujaskta" ><h1 style="color: white;">KLAIPĖDOS <br>TINKLINIO ASOCIACIJA</h1><h2 style="color: white;">Klaipėdos tinklinio bendruomenę jungianti organizacija <br>sieka skatinti tinklinio sportą mieste, <br>garbingai atstovauti Klaipėdos miestą, <br>bei ugdyti jaunuosius <br>sportininkus.</h2></li>


            <li id="amberio">
                <h1>APIE TINKLINIO KOMANDĄ<br>AMBER QUEEN</h1>
                <h2>Klaipėdos tinklinio padangėje sužibusi<br>Amber Queen tinklinio komanda.<br>Jos istorija ir pilna sudėtis<br>nuorodoje žemiau:</h2>
                <a href="pages/amberqueen/amberqueen.php">
                    <div class="buttons">Apie Amber Queen</div>
                </a>
                
            </li>

            <li style="background: linear-gradient(110deg, #ECE9E6, #abbaab);"><h1>NAUJIENA! ELEKTRONINĖS<br>PARAIŠKOS</h1><h2>Didžiausia naujiena tinklapyje - <br>individualios elektroninės paraiškos.<br>Jei dalyvauji Klaipėdos m.<br>tinklinio pirmenybėse -<br>Užpildyk!</h2>
                <a href="pages/komandos/komandos.php">
                    <div class="buttons">Pildyti paraišką!</div>
                </a>
                <img src="images/registration.png"></li>

        </ul>  
            <div class="right">
            <div class="kasnaujo"><a href="pages/kasnaujo/kasnaujo.php"><i class="fa fa-newspaper-o" aria-hidden="true"></i><h2>Kas<br>naujo?</h2></a></div>
            <a href="https://www.facebook.com/Klaipedostinklinis/" target="_blank">
                <div class="akademija">
                    <h2>Sekite mus <br>Facebooke!</h2>
                </div>
            </a>
        </div>
    </div>
    <div class="bottom">
            <div class="mainbutton"><a href="pages/kasnaujo/kasnaujo.php"><h2>Kas<br>Naujo?</h2></a></div>
            <div class="mainbutton"><a href="https://www.facebook.com/Klaipedostinklinis/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i><h2>Sek mūsų<br>Facebook!</h2></a></div>
    </div>
    </wrapper>
</section>

<script src="js/slider.js"></script>

<?php  include 'footer.php'; ?> <!-- =====================footer====================== -->


</body>
</html>