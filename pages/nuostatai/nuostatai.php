<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pirmenybių nuostatai</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
     <?php  include '../header.php'; ?> <!-- =====================header====================== -->

	<section class="second">
        <wrapper>
            <h1>KLAIPĖDOS M. TINKLINIO PIRMENYBIŲ NUOSTATAI</h1>
            <div class="tekstas">

                <h3 style="padding-left: 30px;"><span lang="LT">I. TIKSLAI IR UŽDAVINIAI</span></h3>
                <p><span lang="LT">1. Populiarinti tinklinį ir aktyvų laisvalaikio praleidimo būdą Lietuvos tinklinio mėgėjų tarpe, bei pritraukti kuo daugiau žiūrovų;<br /> 2. Išaiškinti stipriausias 2017-2018 metų Klaipėdos m. tinklinio pirmenybių komandas; <br /> 3. Operatyviai pateikti įvykusių rungtynių rezultatus, <br /> 4. Nušviesti pirmenybių eigą internete ir spaudoje;</span></p>

                <h3 style="padding-left: 30px;"><span lang="LT">II. VADOVAVIMAS IR VYKDYMAS</span></h3>
                <p><span lang="LT">1. Pirmenybes vykdo Klaipėdos tinklinio asociacija, pirmenybių vyr. teisėjas Jonas Miliūnas (861866182), vyr. sekretorius Dainius Skirbutas (869661004)<br /> 2. Pirmenybės vykdomos 2017 m. spalio – 2018 m. kovo mėnesiais organizatorių paskirtose arba visų dalyvaujančiųjų komandų turimose sporto salėse;<br /> 3. Rungtynės žaidžiamos šeštadieniais nuo 14:00 iki 20:00 val. ir sekmadieniais nuo 13:00 val. iki 20:00 val.;</span></p>
                <p><span lang="LT">4. Komandos negalinčios žaisti organizatorių paskirtu laiku, privalo pačios susitarti su priešininkų komanda, teisėjais ir sekretoriais, ir ne vėliau kaip 10 dienų iki varžybų pradžios pranešti vyr. sekretoriui apie pasikeitimus. <br /> 5. Pirmenybėse teisėjaujama pagal oficialias galiojančias 2016-2017 m. FIVB taisykles ir jų papildymus. </span></p>
                
                <p><span lang="LT">6. Pirmenybės vykdomos trijose grupėse: A lyga 2 ratai pogrupyje, B lyga 2 ratai pogrupyje ir Veteranų lyga 1 ratas pogrupyje. </span></p>
                
                <p><span lang="LT">7. Taškai bus skaičiuojami taip: laimėjimas rezultatu 3:0 arba 3:1 – 3 taškai, laimėjimas rezultatu 3:2 – 2 taškai, pralaimėjimas rezultatu 2:3 – 1 taškas, pralaimėjimas rezultatu 0:3 arba 1:3 – 0 taškų.</span></p>
                
                <p><span lang="LT">8. Pasibaigus pogrupių varžyboms, turnyrinėje lentelėje dviems komandoms, surinkus vienodą taškų skaičių, pirmenybė teikiama komandai turinčiai geresnį laimėtų - pralaimėtų setų santykį tarp visų komandų. Jei šis santykis vienodas, vietos nustatomos pagal pelnytų - praleistų taškų santykį tarp visų komandų;</span></p>
                <p><span lang="LT">Atkrintamosios varžybos.</span></p>
                <p><span lang="LT">8. Į atkrintamąsias varžybas patenka A, B ir Veteranų lygose 1-4 vietas užėmusios komandos;<br /> 9. A lygoje pirmas keturias vietas užėmusios komandos finaliniame etape (pusfinaliai ir finalai) kovoja dėl Klaipėdos miesto pirmenybių DIDŽIOSIOS TAURĖS. ((1vt.-4vt.)-(3vt.-2vt.));<br /> 10. B lygoje pirmas keturias vietas užėmusios komandos finaliniame etape (pusfinaliai ir finalai) kovoja dėl Klaipėdos miesto pirmenybių MAŽOSIOS TAURĖS. ((1vt.-4vt.)-(3vt.-2vt.));<br /> 11. Veteranų grupėje pirmas keturias vietas užėmusios komandos finaliniame etape (pusfinaliai ir finalai) kovoja dėl Klaipėdos miesto pirmenybių VETERANŲ 40+ TAURĖS. ((1vt.-4vt.)-(3vt.-2vt.));</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">III. DALYVIAI</span></h3>
                <p><span lang="LT">1. Pirmenybėse 6x6 gali dalyvauti visos vyrų tinklinio komandos ir vaikai sportuojantys sporto mokyklose.<br /> 2. Dalyvis laikomas sužaidusiu rungtynes, jei yra užregistruotas rungtynių protokole ir pabuvojo aikštėje rungtynių metu.<br /> 3. Komandos, dalyvaujančios Lietuvos tinklinio čempionate (aukščiausia ir 1-a lygos), Baltijos lygos tinklinio čempionate, Lietuvos studentų tinklinio čempionate, ar kituose panašaus pajėgumo turnyruose, organizatoriams privalo pateikti užimtumo grafiką, kad nesidubliuotų rungtynės.</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">IV. KOMANDŲ REGISTRACIJA</span></h3>
                <p><span lang="LT">Komandinėje paraiškoje turi būti komandos vadovo, trenerio ir kapitono kontaktiniai telefonų numeriai bei el. paštai. </span></p>
                <p><span lang="LT">Vardinėje paraiškoje turi būti šie duomenys:<br /> 1. Žaidėjo vardas ir pavardė;<br /> 2. Pilna gimimo data (metai-mėnuo-diena);<br /> 3. Parašas dėl traumų ir pakenkimo sveikatai rizikos prisiėmimo;<br /> <br /> Paraiškos dalyvauti Klaipėdos m. tinklinio pirmenybėse turi būti išsiųstos elektroniniu paštu adresu </span><span lang="LT">info@kta.lt</span><span lang="LT">arba pristatytos į organizatoriams, iš anksto pasiskambinus ir susiderinus telefonu </span><span lang="LT">869661004</span><span lang="LT">. Registracija prasideda nuo 2017 metų rugsėjo 5 dienos ir tęsis iki spalio 13 dienos. Organizatoriai pasilieka teisę sustabdyti registraciją anksčiau laiko arba ją pratęsti surinkus numatytam komandų-dalyvių skaičiui.</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">V. REIKALAVIMAI KOMANDOMS</span></h3>
                <p><span lang="LT">1. Dalyviai privalo žinoti "Oficialiąsias Tinklinio Taisykles" ir jų laikytis. Dalyviai privalo sportiškai ir nesiginčydami paklusti teisėjų sprendimams. Kilus abejonėms, prašyti teisėjų paaiškinimų gali tik rungtynių kapitonas varžybų teisėjui leidus.</span></p>
                <p><span lang="LT">2. Komandos privalo organizatorių paskirtu laiku atvykti į varžybas ir pradėti žaisti. Organizatorių paskirtas laikas tvarkaraštyje reiškia varžybų teisėjo švilpuką pirmo seto padavimui. Prieš varžybas komandai skiriamos 10 minučių laikas, kurio turi pakakti komandų narių apšilimui su kamuoliu;<br /> 3. Komandos žaidėjai privalo dėvėti tvarkingą sportinę avalynę;<br /> 4. Komandos žaidėjai privalo vilkėti tvarkingomis, vienodomis (išskyrus „Libero“), sunumeruotomis sportinėmis aprangomis, numeris turi būti aiškiai matomas priekyje ir nugaroje;<br /> 5. Jei komandos tarpusavyje susitaria žaisti kitoje salėje, komanda, salės šeimininkė privalo:<br /> 5.1 Rungtynėms sporto salėje ištempti tinklą;<br /> 5.2 Paruošti sekretoriatui ir rugtynių teisėjams darbo vietas: bokštelį, stalą ir kėdes;<br /> 5.3 Jei salėje yra automatinis ar stalinis tablo, savo nuožiūra paskirti žmones fiksuoti rezultatą;<br /> 5.4 Salėje turėti sukomplektuotą vaistinėlę ir rungtynių metu užtikrinti minimalų medicininį aptarnavimą;<br /> 5.5 Sutikti ir palydėti rungtynių teisėjus, bei užtikrinti jų saugumą.<br />5.6. Savarankiškai susimokėti už salės nuomą, teisėjų ir sekretoriato darbą.<br />6. Kiekvienas komandos žaidėjas privalo pasirašyti komandos vardinėje paraiškoje. Žaidėjas, pasirašydamas paraišką patvirtina ir garantuoja, kad jo sveikatos būklė yra tinkama dalyvauti tinklinio varžybose, žaidėjas dėjo pakankamas ir protingas pastangas, kad jo sveikatos būklė būtų patikrinta kompetentingų specialistų ir patvirtinta kompetentingų specialistų išvada, žaidėjo sveikatos būklė bus tinkama visą laikotarpį, kol jis dalyvauja lygos organizuojamose varžybose, žaidėjas, esant reikalui, reguliariai tikrinsis savo būklę, o jei paaiškės ar kils bent mažiausių abejonių, kad žaidėjas negali dalyvauti lygos organizuojamose varžybose, pats, savo iniciatyva nutrauks dalyvavimą lygos organizuojamose varžybose. Jei žaidėjo pareiškimai ir garantijos yra ar taps neteisingi, žaidėjas, sąmoningai ar dėl neatsargumo tikrindamasis sveikatos būklę nuslėps bet kokias aplinkybes, galinčias lemti žaidėjui neigiamas pasekmes ar dėl kurių žaidėjas nebūtų gavęs kompetentingų specialistų sprendimo dalyvauti varžybose, žaidėjas, pasirašydamas komandos vardinėje paraiškoje patvirtina, kad pats savo valia prisiima visas galimas neigiamas pasekmes.<br />7. Organizatoriai neatsakingi už žaidėjų sveikatos būklę ir galimus jos sutrikimus varžybų metu.</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">VI. ŽAIDĖJAI</span></h3>
                <p><span lang="LT">1. Komanda paraiškoje turi registruoti ne mažiau kaip 6-is ir ne daugiau kaip 18 žaidėjų;<br /> 2. Komandos vadovas vardinę paraišką privalo pateikti sekretoriatui prieš pirmąsias komandos rungtynes. Neregistruotiems žaidėjams rungtyniauti draudžiama;<br /> 3. Prasidėjus pirmenybėms, iki lapkričio 17 dienos naujai užregistruotam žaidėjui, registracijos mokestis 15 €;<br /> 5. Žaidėjai užregistruoti ir sužaidę bent vienas rungtynes už kurią nors komandą, nebegali keisti komandos viso sezono metu. Pereiti iš vienos komandos į kitą sezono metu galima tik gruodžio 15-31 dienomis, apie tai raštiškai pranešus varžybų vyriausiam sekretoriui. Prie prašymo pereiti į kitą komandą žaidėjas privalo pateikti abiejų komandų vadovų, laisvos formos sutikimą ir 15 € pavedimo į organizatorių sąskaitą banke, kopiją;</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">VII. DRAUSMINĖS NUOBAUDOS IR PROTESTAI</span></h3>
                <p><span lang="LT">1. Žaidėjui gavus kartu geltoną+raudoną korteles skiriama 7 € bauda, kurią jis privalo susimokėti iki sekančių rungtynių. Jei žaidėjas nesusimokėjęs baudos sužaidžia sekančias rungtynes, jo komandai įskaitomas techninis pralaimėjimas 0:3;<br /> 2. Komandai esant ne pilnos sudėties, priešininkai turi pilną teisę atsisakyti tęsti/pradėti varžybas. Tokiu atveju nepilnos sudėties komandai įskaitomas techninis pralaimėjimas 0:3. <br /> 3. Visus mokesčius reikia atlikti pavedimu pagal apačioje nuostatų nurodytus įmonės rekvizitus ir sąskaitą, įmokos vietoje įrašant mokėtojo vardą ir pavardę ir prieš rungtynes sekretoriatui pristatyti apie atliktą pavedimą patvirtinantį dokumentą.<br /> 4. Žaidėjas negalės žaisti 2018-2019 metų sezone, jei liko nesusimokėję praeitų metų sezone gautą techninę pražangą, nepriklausomai nuo to, koks tai buvo turnyras, pirmenybės ar atstovaujama komanda;<br /> 5. Organizatoriams, gavus skundą dėl žaidėjo ar komandos nusižengimų, agresyvaus elgesio teisėjų, varžovų ar sekretoriato atžvilgiu, gali būti panaudojamos griežtos sankcijos, skiriant pinigines nuobaudas iki 60 €, kartu su diskvalifikacija kelioms rungtynėms. Įvykus dar rimtesniems nusižengimams bus svarstoma apskritai apie tokios komandos ar žaidėjo tolimesnį dalyvavimą pirmenybėse. Diskvalifikavus komandą startinis mokestis negrąžinamas.<br /> 6. Pasibaigus rungtynėms, protestuojančios komandos kapitonas nedelsiant turi pranešti rungtynių vyr. teisėjui, kad jo komanda protestuoja rungtynių rezultatą;<br /> 7. Protestas pateikiamas iškarto pasibaigus rungtynėms kartu su 30 € užstatu;<br /> 8. Protestą rašo komandos vadovas (treneris), nurodytas paraiškoje, išdėstant aplinkybes, dėl kurių skundžiamasi ir su kuriomis nesutinkama. Kito asmens pasirašyti protestai nepriimami ir nesvarstomi;<br /> 9. Patenkinus protestą užstatas gražinamas, o jei sprendimas nekeičiamas ir rezultatas paliekamas toks pat, pinigai negražinami;<br /> 10. Dėl teisėjavimo protestas priimamas tik pateikus vaizdinę filmuotą medžiagą;<br /> 11. Pirmenybių organizatoriai protestą privalo svarstyti ir sprendimą priimti ne vėliau kaip per 5 paras nuo įteikimo pradžios;<br /> 12. Pirmenybių organizatorių sprendimai yra galutiniai ir neskundžiami.</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">VIII. APDOVANOJIMAI</span></h3>
                <p><span lang="LT">1. </span><span lang="LT"> Komandas prizininkes bus apdovanotos taurėmis ir diplomais;<br />2. </span><span lang="LT"> Komandos prizininkės kiekvienas narys bus apdovanotas medaliu.</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">IX. STARTINIS MOKESTIS</span></h3>
                <p><span lang="LT">1. Komandos iki 2017 m. spalio 15 d. privalo susimokėti pirmenybių startinį mokestį: A lyga- 250 €; B lyga – 200 €; Veteranų lyga – 150 €.<br />2. Visi dalyvių surinkti pinigai bus skirti varžybų organizavimui ir prizams steigti.</span></p>
                <h3 style="padding-left: 30px;"><span lang="LT">X. ORGANIZATORIŲ REKVIZITAI:</span></h3>

                <div class="rekvizitai">
                    <p><span lang="LT"><strong>VŠĮ KLAIPĖDOS TINKLINIO ASOCIACIJA</strong></span><span lang="LT"><br /> Įm. k. 302843365<br /> Vakario g. 15, Ginduliai,<br /> Klaipėdos rajonas.<br /> Atsiskaitomoji sąskaita<br /> Nr. LT107044060007892226<br /> AB SEB bankas<br /> Banko kodas 70440</span></p>
                </div>
                </div>
</wrapper>
</section>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

    
</body>
</html>