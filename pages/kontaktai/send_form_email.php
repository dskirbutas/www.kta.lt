<?php
if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "info@kta.lt";
    $email_subject = "Susisiekime. Gauta žinutė iš kta.lt";
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['first_name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['comments'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
    $first_name = $_POST['first_name']; // required
    $email_from = $_POST['email']; // required
    $comments = $_POST['comments']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'Netinkamas el. pašto adresas.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(strlen($comments) < 2) {
    $error_message .= 'Jūsų žinutė neatitinka reikalavimų.<br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Nuo: ".clean_string($first_name)."\n";
    $email_message .= "El. paštas: ".clean_string($email_from)."\n";
    $email_message .= "Žinutė: ".clean_string($comments)."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
?>
 
<!-- include your own success html here -->
 <html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Klaipėdos tinklinio asociacija</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body style="background: url(../../images/bgxl.jpg);
    background-position: center 0;
    background-repeat:  no-repeat;
    background-size:  cover;">
	<header>
        <wrapper>
            <a href="../../index.php"><img src="../../images/logo.png"></a>
            <li><a href="../kontaktai/kontaktai.php">Kontaktai</a></li>
            <li><a href="../rezultatai/rezultatai.php">Rezultatai</a></li>
            <li><a href="../tvarkarastis/tvarkarastis.php">Tvarkaraštis</a></li>
            <li><a href="../../index.php">Pradžia</a></li>
        </wrapper>
    </header>
	<div class="modal" style="display: block; top: 25vh;">
		<div class="modal-content" style="
		  background: linear-gradient(to bottom, #f7b733, #fe8c00, #f83600);
		  margin: auto;
		  width: 90%;
		  max-width: 450px;
		  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);">
		    <div class="row setup-content">
				<h1 style="font-family: 'Montserrat', sans-serif;
					font-weight: 900;
					font-size: 3.6rem;
					margin: 2%;
					margin-top: 20px;
					text-align: center;">AČIŪ!
				</h1>
				<h3 style="margin: 2rem 5%;text-align: center;">Artimiausiu metu susisieksime su Jumis nurodytu el. paštu.</h3>
				<button class="btn nextBtn btn-lg" type="button" style="
				    background-color: rgba(0,0,0,0);
				    border: 2px solid #00C0C6;
				    border-radius: 0.6em;
				    color: white;
				    font-family: 'Exo', sans-serif;
				    font-weight: 600;
				    text-align: center;
				    text-shadow: 0px 0px 6px #000000;
				    box-shadow: 0px 0px 6px #000000;
				    line-height: 15px;
				    margin: 10px;
				    display: block;
				    margin: 10px auto;
				 	"><a style="color: white; text-decoration: none;" href="kontaktai.php">Grįžti atgal</a>
				</button>
		    </div>
		</div>
	</div>
</body>
</html>
<?php
 
}
?>