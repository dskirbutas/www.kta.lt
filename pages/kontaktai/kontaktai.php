<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Kontaktai</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
       function onSubmit(token) {
         document.getElementById("contactus").submit();
       }
     </script>
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
    <?php  include '../header.php'; ?> <!-- =====================header====================== -->

	<section class="second">
        <wrapper>
            <h1>KONTAKTAI</h1>

            <div class="leftas">
            <div class="inline">
            <div class="teammember" style="margin-bottom: 10px;">
                <div class="teammember__img" title="Image of Dainius Blazevicius"></div>
                <div class="teammember__content">
                    <h2 class="teammember__content__name"> Dainius Blaževičius</h2>
                    <h3 class="teammember__content__job"> Prezidentas</h3>
                    <ul class="teammember__content__contact">
                        <li id="phone" data-clipboard-text="+370 698 10140" class="teammember__content__contact__phone"><a href="tel:+37069810140">+370 698 10 140</a></li>
                        <li id="email" data-clipboard-text="tinklinioasociacija@gmail.com" class="teammember__content__contact__email"><a href="mailto:tinklinioasociacija@gmail.com">tinklinioasociacija@gmail.com</a></li>
                        <li id="slack" data-clipboard-text="tim" class="teammember__content__contact__slack"><a target="_blank" href="https://www.facebook.com/dainius.blazevicius">dainius.blazevicius</a></li>
                    </ul>
                </div>
            </div>
            </div>
            <div class="inline">
            <div class="teammember" style="margin-bottom: 10px;" >
                <div style="background:url( '../../images/dddd.jpg'); background-size: cover;" class="teammember__img" title="Image of Skirbutas">
                </div>
                <div class="teammember__content">
                    <h2  class="teammember__content__name"> Dainius Skirbutas
                    </h2>
                    <h3 class="teammember__content__job"> Koordinatorius
                    </h3>
                    <ul class="teammember__content__contact">
                        <li id="phone" data-clipboard-text="+370 698 10140" class="teammember__content__contact__phone"><a href="tel:+37069661004">+370 69 66 1004</a>
                        </li>
                        <li id="email" data-clipboard-text="tinklinioasociacija@gmail.com" class="teammember__content__contact__email"><a href="mailto:info@kta.lt">info@kta.lt</a>
                        </li>
                        <li id="slack" data-clipboard-text="tim" class="teammember__content__contact__slack"><a target="_blank" href="https://www.facebook.com/dskirbutas">dskirbutas</a>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
            </div>
            <div class="middle">
            <div class="teammember teammember2">
                <div class="teammember__IM" title="Image of Tim Mueller"></div>
                <div class="teammember__content">
                    <h2 class="teammember__content__office">REKVIZITAI</h2>
                    <h3 class="teammember__content__job"> VšĮ "Klaipėdos Tinklinio Asociacija"</h3>
                    <h3 class="teammember__content__job"> Paramos gavėjo kodas: 302843365</h3>
                    <h3 class="teammember__content__job"> Vakario g. 15, Ginduliai, Klaipėdos r.</h3>
                    <h3 class="teammember__content__job" style="padding-bottom: 32px;"> Atsiskaitomoji sąskaita: LT107044060007892226</h3>
                </div>
            </div>
            </div>
                <div class="right" style="">
                    <form id="contactus" class="contact" name="contactform" method="post" action="send_form_email.php" accept-charset='UTF-8' autocomplete="off">
                      <h4>SUSISIEKIME!</h4>
                      <div class="group">
                        <input type="text" id="name" required="required" maxlength="20" name="first_name" />
                        <label for="name">Vardas</label>
                      </div>
                      <div class="group">
                        <input type="text" id="email" required="required" maxlength="30" name="email" />
                        <label for="email">El. paštas</label>
                      </div>
                      <div class="group" style="width: 90%; margin: 0 5%;">
                        <textarea id="message" required="required" maxlength="1000" name="comments"></textarea>
                        <label for="message">Jūsų žinutė</label>
                      </div>
                      <button class="g-recaptcha" data-sitekey="6LeRd0UUAAAAAHjt3IQcwTSbDEdPLu_Qj0oavqac" data-callback='onSubmit' type="submit" value="submit"></button>
                    </form>
                </div>
    </wrapper>
</section>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

    
</body>
</html>