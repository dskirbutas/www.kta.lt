<!DOCTYPE html>
<html>
<head>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tinklinio turnyrai Klaipėdoje</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
<?php  include '../header.php'; ?> <!-- =====================header====================== -->

	<section class="second">
        <wrapper>
            <h1>TINKLINIO TURNYRAI</h1>
                    <div class="header"><h2>Klaipėdos miesto tinklinio pirmenybės nors ir didžiausias ir svarbiausias tinklinio renginys mieste, tačiau tikrai ne vienintelis. Kviečiame susipažinti ir su kitais Klaipėdoje vykstančiais tinklinio turnyrais:</h2></div>

                    <div class="turnyras">
                        <div class="tablecontainer">
                            <div class="top" id="gitana"></div>
                            <p>Kiekvieną rudenį vykstantis Gitanos taurės turnyras, remiamas įmonės "GITANA", kasmet pritraukia vis daugiau komandų iš visos Lietuvos. Jubiliejinis, penktasis turnyras išaugo į vieną didžiausių šalyje, jame varžėsi net 21 komanda!</p>
                            <div class="maininfo">
                                <ul class="list-unstyled">
                                   <li><p>Pradžia - nuo 2002 m.</p></li>
                                   <li><p>Data - Spalio 1 pusė</p></li>
                                   <li><p>Dalyviai - pažengę mėgėjai</p></li>
                                   <li><p>Dalyvių sk. - 21 komanda</p></li>
                                </ul>
                          </div>
                      </div>
                   </div>
                    <div class="turnyras">
                      <div class="tablecontainer">
                        <div class="top" id="amans"></div>
                          <p>AMANS - žiemos pabaigoje vykstantis turnyras, prasidėjęs nuo merginų megėjų mini turnyrėlio, užaugo į 12 komandų, plačios geografijos, visos dienos turnyrą, pasižyminti sklandžia organizacija, puikiais prizais ir gera nuotaika.</p>
                          <div class="maininfo">
                          <ul class="list-unstyled">
                            <li><p>Pradžia - nuo 2004 m.</p></li>
                            <li><p>Data - vasario pabaiga</p></li>
                            <li><p>Dalyviai - mėgėjai</p></li>
                            <li><p>Dalyvių sk. - 12 komandų</p></li>
                         </ul>
                      </div>
                      </div>
                    </div>
                    <div class="turnyras">
                        <div class="tablecontainer">
                        <div class="top" id="galaksa"></div>
                          <p>Ilgametis ir tradicinis "GALAKSA" tinklinio turnyras, trunkantis 2 mėnesius pritraukia visus Klaipėdos tinklinio mėgėjus-pradedančiuosius pasivaržyti tarpusavyje. Varžybos vyksta M. Gorkio mokyklos sporto salėje. Organizuoja Oksana Jolkina </p>
                          <div class="maininfo">
                          <ul class="list-unstyled">
                            <li><p>Pradžia - nuo 2008 m.</p></li>
                            <li><p>Data - Spalis-lapkritis</p></li>
                            <li><p>Dalyviai - Klaipėdos mėgėjai</p></li>
                            <li><p>Dalyvių sk. - 12-14 komandų</p></li>
                         </ul>
                      </div>
                      </div>
                    </div>
                    <div class="turnyras">
                      <div class="tablecontainer" id="margiene">
                          <div class="top"><h3 style="letter-spacing: 1px;">V.Margienės taurė</h3></div>
                          <img src="../../images/margiene.jpg">
                          <p>Tarptautinis turnyras, skirtas Virginijai Margienei, daugkartinei Lietuvos čempionei, ir eilei kitų turnyrų prizininkei, pagerbti. Turnyras pritraukia ir kaimyninių šalių veteranų komandas pasivaržyti Klaipėdoje. Organizuoja sporto klubas Viesulas.</p>
                          <div class="maininfo">
                          <ul class="list-unstyled">
                            <li><p>Pradžia - nuo 2006 m.</p></li>
                            <li><p>Data - Gruodžio pradžia</p></li>
                            <li><p>Dalyviai - Veteranai</p></li>
                            <li><p>Dalyvių sk. - 18 komandų</p></li>
                         </ul>
                      </div>
                      </div>
                    </div>
                    <div class="turnyras">
                      <div class="tablecontainer" id="usas">
                        <div class="top"><h3>Ūso turnyras</h3></div>
                          <img src="../../images/usas.jpg">
                          <p>Šventinis, kupinas linksmos nuotaikos tinklinio turnyras, tradiciškai rengiamas sausio 1 dieną. Turnyro talismanas, įkvėpėjas, ir garbės svečias Vardas Pavardė aka Ūsas. Į turnyrą kviečiami visi pavieniai tinklininkai, vietoje sudaromos komandos.</p>
                          <div class="maininfo">
                          <ul class="list-unstyled">
                            <li><p>Pradžia - nuo 2004 m.</p></li>
                            <li><p>Data - Sausio 1 d.</p></li>
                            <li><p>Dalyviai - visi gyvi</p></li>
                         </ul>
                      </div>
                      </div>
                    </div>
                    <div class="turnyras">
                      <div class="tablecontainer" id="fallcup">
                        <div class="top"><h3>Fall Cup</h3></div>
                          <img src="../../images/fallcup.jpg">
                          <p>Rudeninis tinklinio turnyras, skirtas pasiruošti Klaipėdos m. tinklinio pirmenybėms. Įvairios miesto komandos išmėgina jėgas po vasaros pertraukos, atgaivina įgudžius bei išbando naujokus. </p>
                          <div class="maininfo">
                          <ul class="list-unstyled">
                            <li><p>Pradžia - nuo 2004 m.</p></li>
                            <li><p>Data - Spalis</p></li>
                            <li><p>Dalyviai - Klaipėdos A lyga</p></li>
                            <li><p>Dalyvių sk. - 5 komandos</p></li>
                         </ul>
                      </div>
                      </div>
                    </div>
        
    </wrapper>
</section>
 <?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

</body>
</html>