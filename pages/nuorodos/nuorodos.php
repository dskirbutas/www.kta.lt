<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tinklinio taisyklės Naudingos nuorodos</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
<?php  include '../header.php'; ?> <!-- =====================header====================== -->

	<section class="second">
        <wrapper>
           <div class="container">
                <div class="col-sm-6 col-xs-12">
                    <h1>Naudingos nuorodos</h1>
                    <table class="table">
                      <tbody class="tablecontent">
                     <tr>
                      <td><a href="http://www.ltf.lt" target="_blank"><p>Lietuvos tinklinio federacija</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.gargzdusc.lt" target="_blank"><p>Gargždų sporto centras</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.pliazotinklinis.lt" target="_blank"><p>Tinklinio klubas AUKSMA</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.ktml.lt" target="_blank"><p>Kauno tinklinio mėgėjų lyga</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.tinklinioakademija.lt" target="_blank"><p>Tinklinio akademija Vilniuje</p></td>
                     </tr>
                        <tr>
                      <td><a href="http://www.360sport.lt/" target="_blank"><p>Vilniaus Terrasport tinklinio lygos (360sportas)</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.tinklasvilnius.lt" target="_blank"><p>Vilniaus sporto klubas Tinklas</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.sportasvisiems.lt" target="_blank"><p>Lietuvos asociacija "Sportas visiems"</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.sportas.info" target="_blank"><p>Sportas.info</p></td>
                     </tr>
                       <tr>
                      <td><a href="http://vmtf.lt/" target="_blank"><p>Vilniaus miesto tinklinio čempionatas</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.com" target="_blank"><p>Federation Internationale de Volleyball (FIVB)</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.cev.lu" target="_blank"><p>Confédération Européenne de Volleyball (CEV)</p></td>
                     </tr>


                    </tbody>
                    </table>
                      </div>
                <div class="col-sm-6 col-xs-12">
                    <h1>Naudinga literatūra</h1>
                    <table class="table">
                      <tbody class="tablecontent">
                         <tr>
                      <td><a href="http://www.ltf.lt" target="_blank"><p>Salės tinklinio taisyklės</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.ltf.lt" target="_blank"><p>Paplūdimio Tinklinio taisyklės</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/documents/FIVB-Volleyball_Rules_2017-2020-EN-v06.pdf" target="_blank"><p>Official Volleyball Rules (2017-2020)</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/Documents/FIVB-BeachVolleyball_Rules_2017-2020-EN-v05.pdf" target="_blank"><p>Official Beach Volleyball Rules (2017-2020)</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/documents/FIVB-Volleyball_Rules_2017-2020-RU-v01.pdf" target="_blank"><p>ВОЛЕЙБОЛЬНЫЕ ПРАВИЛА (2017-2020)</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/Documents/FIVB-BeachVolleyball_Rules_2017-2020-RU-v01.pdf" target="_blank"><p>ПРАВИЛА ПЛЯЖНОГО ВОЛЕЙБОЛА (2017-2020)</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/Documents/FIVB_VB_Refereeing_Guidelines_and_Instructions_fv.26.03.2017.pdf" target="_blank"><p>Refereeing Guidelines and Instructions</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/Documents/FIVB_VB_Scoresheet_2013_updated2.pdf" target="_blank"><p>Official Volleyball Scoresheet</p></td>
                     </tr>
                       <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/Documents/2017_Volleyball_Rules_Casebook_v1_15.03.2017.pdf" target="_blank"><p>FIVB Volleyball Casebook</p></td>
                     </tr>
                      <tr>
                      <td><a href="http://www.fivb.org/EN/Refereeing-Rules/Documents/2017_Beach_Volleyball_Rules_Casebook_v21.04.2017.pdf" target="_blank"><p>FIVB Beach Volleyball Casebook</p></td>
                     </tr>
                    </tbody>
                    </table>
                  </div>
  </div>
     
    </wrapper>
</section>
   <?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

</body>
</html>