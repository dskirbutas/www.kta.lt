<html>
<head>


    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>


    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../../favicon.ico" type="image/x-icon">
	<title>Klaipėdos tinklinio naujienos</title> 
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
     <?php  include '../header.php'; ?> <!-- =====================header====================== -->
	<section class="second">
        <wrapper>
            <h1>Kas Naujo?</h1>

            <time datetime="2018-04-21">04 21</time>
            <p>Pasibaigė 2017-2017 m. Klaipėdos tinklinio pirmenybės, finaluose, kaip ir tikėtasi, įnirtingos kovos, išaiškinusios šių metų stipriausius: A lyga - Amber Queen, KU, Gitana. B lyga - Šilas-SM, Judrėnų Grensena, Giruliai. Veteranai - Vakaris, Gitana, Plungė. D lyga - Viesulas, Gargždų SC, Plungės SRC. Sveikinam nugalėtojus!</p>

            <hr>
            <time datetime="2018-03-20">03 20</time>
            <p>Pagaliau į pasaulį išleistas naujas, ilgai brandintas kta.lt tinklapis, gražus, išmanus, patogus ir greitas. Palengvinantis darbą, su elektronine paraiškų teikimo sistema, visi kas dalyvauja miesto pirmenybėse kviečiam išbandyti!</p>

            <hr>
            <time datetime="2018-03-11">03 11</time>
            <p>Lietuvos nepriklausomybės atkūrimo diena - šventinė diena visiems lietuviams, tačiau tinklinio mėgėjams tai dviguba šventė, nes Kovo 11 d. atidaryta ir Dailioji lyga, kurioje septynios merginų-moterų komandos. Jau sužaistos septynios varžybos, dar daugiau jų laukia ateityje, sekite tvarkaraštį ir ateikite pasigrožeti!
            </p>

            <hr>
            <time datetime="2018-03-10">03 10</time>
            <p>Kovo 10d. Klaipėdos Tinklinio Akademijos mergaitės dalyvavo Plungėje vykusiame merginų U20 salės tinklinio turnyre, ir į Klaipėdą grįžo su iškovota 3 vieta. Sveikinimai mergaitėms pasisėmus vertingos patirties ir nuskynus medalius. Dėkojame organizatoriams.</p>

            <hr>
            <time datetime="2018-02-25">02 24</time>
            <p>Vasario 24 d. Klaipėdoje vyko tradicinis "Amans" salės tinklinio turnyras, kuriame susikovė 5 vyrų ir 10 moterų komandos. Tarp vyrų stipriausi buvo klaipėdiečiai Gitanos tinklininkai, antri Gargždai, treti Kėdainių "Laumetrio" komanda. Moterų A grupėje pirmąją vietą užėmė Klaipėdos komanda, antrosios liko klaipėdos "Viesulo" komanda , trečią vietą iškovojo Jonava. Moterų B grupėje vėl pirmąją vietą iškovojo Klaipėdos komanda, po jų Rietavas, ir trečios Kauno "Vitameda" komanda. Po varžybų vykusioje apdovanojimų ceremonijoje buvo paskelbti ir geriausi komandų žaidėjai. Jie buvo apdovanoti turnyro rėmėjų – „Memel Amber“ apyrankėmis, skirtomis butent šiam turnyrui. Nugalėtojų komandoms prizus įsteigė AB "Klaipėdos Baldai" bei "4Corners".</p>


    </wrapper>
</section>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

</body>
</html>