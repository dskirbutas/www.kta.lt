$('#komandos').change( function(){
  var selection = $(this).val();
  var dataset = $('#table tbody').find('tr');
  
  dataset.each(function(index) {
    item = $(this);
    item.hide();
    
    var firstTd = item.find('td:nth-child(4)');
    var text = firstTd.text();
    var ids = text.split(' - ');
    
    for (var i = 0; i < ids.length; i++)
    {
    	if (ids[i] == selection)
      {
      	item.show();
      }
      else if (selection == "Rodyti visus") {
      item.show();
    } 
    }

  });
});

$('.radio-button').change( function(){
  var selection = $(this).val();
  var dataset = $('#table tbody').find('tr');
  
  dataset.each(function(index) {
    item = $(this);
    item.hide();
    
    var firstTd = item.find('td:first-child');
    var text = firstTd.text();
    var ids = text.split(' - ');
    
    for (var i = 0; i < ids.length; i++)
    {
      if (ids[i] == selection)
      {
        item.show();
      }
    }

  });
});






