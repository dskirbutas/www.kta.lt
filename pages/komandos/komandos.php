<?php
define('DB_SERVER', 'localhost');
define('DB_USER', 'bschemic_kta');
define('DB_PASS', 'Dainius21');
define('DB_NAME', 'bschemic_kta');


$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
$conn->set_charset("utf8");
if($conn->connect_error) {
  echo 'Klaida: $conn->connect_error';
} 



?>


<!DOCTYPE html>
<html>
<head>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Komandos</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    

    
</head>
<body>
  <button class="hamburger" style="display: none;">&#9776;</button>
  <div class="meniu"><h2>MENIU</h2></div>
    <header>
        <wrapper>
            <a href="../../index.php"><img src="../../images/logo.png"></a>
            <ul>
              <li><a href="../../index.php">Pradžia</a></li>
              <li><a href="../tvarkarastis/tvarkarastis.php">Tvarkaraštis</a></li>
              <li><a href="../rezultatai/rezultatai.php">Rezultatai</a></li>
              <li><a href="../kontaktai/kontaktai.php">Kontaktai</a></li>  
            </ul>
        </wrapper>
    </header>
<section class="second">
<div class=" owl-one owl-carousel owl-theme" id="A">
  

  <div class="table-responsive">
    <h1>AMBER QUEEN</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th title="Sezonai komandoje" >Kartu</th>
          <th title="Anksčiau žaidė">Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

        <?php
          $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
 while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Amber Queen"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>
  <div class="table-responsive">
    <h1>GITANA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Gitana"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
    <h1 class="klpuni">Klaipėdos Universitetas</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Klaipėdos Universitetas"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

 <div class="table-responsive">
   <h1>VAKARIS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "A" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Vakaris"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>KLAIPĖDOS BALDAI</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Klaipėdos Baldai"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
    <h1>PAJŪRIS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Pajūris"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>ALL BLACKS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="All Blacks"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>ŠILAS-SM</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Šilas-SM"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>GRENSENA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Grensena"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>GIRULIAI</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Giruliai"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1 class="klpuni">Klaipėdos Tinklinio Akademija</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="KTA"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>VT KLAIPĖDA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="VT Klaipėda"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>ONIKS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Oniks"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
    <h1>PLUNGĖ</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Plungė"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

   <div class="table-responsive">
   <h1>GITANA <span>V</span></h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "V" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Gitana"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>
  <div class="table-responsive">
    <h1>MORGAN</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Morgan"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

   <div class="table-responsive">
   <h1>VAKARIS <span>V</span></h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "V" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Vakaris"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
   <h1>GIRULIAI <span>V</span></h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimęs</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "V" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Giruliai"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
   <h1>KU<span>D</span></h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Klaipėdos Universitetas"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
   <h1>VAKARŲ TINKLINIS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Vakarų tinklinis"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>



<div class="table-responsive">
   <h1>GARGŽDŲ SC</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Gargždų SC"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
   <h1 class="klpuni">GARGŽDŲ SC MĖGĖJOS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Gargždų SC mėgėjos"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

    <div class="table-responsive">
   <h1>SK VIESULAS</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Viesulas"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

<div class="table-responsive">
   <h1>KLAIPĖDA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Klaipėda"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
   <h1>PLUNGĖS SRC</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <th>Nr.</th>
          <th>Vardas Pavardė</th>
          <th>Ūgis</th>
          <th>Gimusi</th>
          <th>Kartu</th>
          <th>Anksčiau</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
  $sql = 'SELECT * FROM zaidejai WHERE lyga = "D" ORDER BY numeris';
  $result = $conn->query($sql); 
  while ($array = $result->fetch_assoc()) {
    if( $array['komanda'] ==="Plungė"){
    echo "<tr>";
      echo "<td>".$array['numeris']."</td>";
      echo "<td>".$array['vardas']." ". $array['pavarde']."</td>";
      echo "<td>".$array['ugis']." cm"."</td>";
      echo "<td>".$array['gimimo']."</td>";
      echo "<td>".$array['sezonai']." m."."</td>";
      echo "<td>".$array['ankstesne']."</td>";
    echo "</tr>";
  }
}
?>
      </tbody>
    </table>
  </div>

</div>

<div class="pridek" id="myBtn">Pridėk save!</div>
</section>


<!-- ==========================MODAL=================================== -->


<div id="myModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="stepwizard">
      <div class="stepwizard-row setup-panel">
          <div class="stepwizard-step">
              <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
              <p>Vardas</p>
          </div>
          <div class="stepwizard-step">
              <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
              <p>Komanda</p>
          </div>
          <div class="stepwizard-step">
              <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
              <p>Duomenys</p>
          </div>
           <div class="stepwizard-step">
              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
              <p>Kontaktas</p>
          </div>
      </div>
    </div>
    <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method='GET'>
      <div class="row setup-content" id="step-1">
          <div class="col-xs-12">
              <div class="col-md-12">
                <h2>Klaipėdos m. tinklinio pirmenybių</h2>
                <h1>INDIVIDUALI VARDINĖ PARAIŠKA</h1>
                <h3>Užpildęs laukelius tapsi pilnateisiu savo komandos nariu ir galėsi dalyvauti pirmenybėse!</h3>
                <div class="form-group">
                      <label class="control-label">Vardas</label>
                      <input name = "vardas" maxlength="10" type="text" required="required" class="form-control" placeholder="Įveskite savo vardą"  />
                </div>
                <div class="form-group">
                      <label class="control-label">Pavardė</label>
                      <input name = "pavarde" maxlength="15" type="text" required="required" class="form-control" placeholder="Įveskite savo pavardę" />
                </div>
                <button class="btn nextBtn btn-lg pull-right" type="button" >Toliau</button>
              </div>
          </div>
      </div>
      <div class="row setup-content" id="step-2">
          <div class="col-xs-12">
              <div class="col-md-12">
                  <h3>Pasirinkite komandą, kuriai priklausote, lygą, marškinėlių numerį, nurodykite kiek sezonų jau esate šios komandos narys (imtinai) bei komandą, kurioje žaidėte anksčiau.</h3>
                  <div class="form-group">
                      <label class="control-label">Komanda</label>
                      <select  name = "komanda" maxlength="20" type="text" required="required" class="form-control">
                          <option style="display: none;">Pasirinkite savo komandą</option>
                          <option>Amber Queen</option>
                          <option>Gitana</option>
                          <option>Klaipėdos Universitetas</option>
                          <option>Vakaris</option>
                          <option>Klaipėdos Baldai</option>
                          <option>Pajūris</option>
                          <option>All Blacks</option>
                          <option>Šilas-SM</option>
                          <option>Grensena</option>
                          <option>Giruliai</option>
                          <option>KTA</option>
                          <option>VT Klaipėda</option>
                          <option>Oniks</option>
                          <option>Plungė</option>
                          <option>Morgan</option>
                          <option>Vakarų tinklinis</option>
                          <option>Gargždų SC</option>
                          <option>Gargždų SC mėgėjos</option>
                          <option>Klaipėda</option>
                          <option>Viesulas</option>
                      </select>
                </div>
                <div class="form-group">
                      <label class="control-label">Pasirinkite lygą:</label>
                        <div class="radio-tile-group">
                          <div class="input-container">
                            <input id="walk" class="radio-button" type="radio" name="radio" value="A" />
                            <div class="radio-tile">
                              <h1>A</h1>
                            </div>
                          </div>

                          <div class="input-container">
                            <input id="bike" class="radio-button" type="radio" name="radio" value="B" />
                            <div class="radio-tile">
                              <h1>B</h1>

                            </div>
                          </div>

                          <div class="input-container">
                            <input id="drive" class="radio-button" type="radio" name="radio" value="V" />
                            <div class="radio-tile">
                              <h1>V</h1>

                            </div>
                          </div>

                          <div class="input-container">
                            <input id="fly" class="radio-button" type="radio" name="radio" value="D" />
                            <div class="radio-tile">
                              <h1>D</h1>

                            </div>
                          </div>
                        </div>
                  </div>
                <div class="form-group dviese">
                  <label class="control-label">Numeris</label>
                  <div class='ctrl'>
                    <div class='ctrl-button ctrl-button-decrement'>-</div>
                    <div class='ctrl-counter'>
                      <input  name = "numeris" class='ctrl-counter-input numeris' maxlength='2' type='text' value='0'>
                        <div class='ctrl-counter-num'>0</div>
                    </div>
                    <div class='ctrl-button ctrl-button-increment'>+</div>
                  </div>
                </div>
                  <div class="form-group dviese">
                    <label class="control-label">Sezonas</label>
                    <div class='ctrl'>
                      <div class='ctrl-button ctrl-button-decrement pirmas'>-</div>
                      <div class='ctrl-counter antras'>
                        <input name = "sezonai" class='ctrl-counter-input ketvirtas' maxlength='2' type='text' value='1'>
                        <div class='ctrl-counter-num trecias'>1</div>
                      </div>
                      <div class='ctrl-button ctrl-button-increment penktas'>+</div>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label">Prieš tai buvusi komanda</label>
                      <input name = "ankstesne" maxlength="20" type="text" required="required" class="form-control" placeholder="Įrašykite komandą" />
                  </div>
                  <button class="btn prevBtn btn-lg pull-left" type="button" >Atgal</button>
                  <button class="btn nextBtn btn-lg pull-right" type="button" >Toliau</button>
              </div>
          </div>
      </div>
      <div class="row setup-content" id="step-3">
          <div class="col-xs-12">
              <div class="col-md-12">
                  <h2 style="margin: 10px;">Fiziniai duomenys</h3>
                  <h3>Dažniausiai paraiškose sutinkami duomenys:</h3>
                  <div class="form-group dviese">
                    <label class="control-label centered">Ūgis</label>
                    <div class='ctrl'>
                      <div class='ctrl-button ctrl-button-decrement vienas'>-</div>
                      <div class='ctrl-counter du'>
                        <input name = "ugis" class='ctrl-counter-input keturi' maxlength='3' type='text' value='185'>
                        <div class='ctrl-counter-num trys'>185</div>
                      </div>
                      <div class='ctrl-button ctrl-button-increment penki'>+</div>
                    </div>
                  </div>
                  <div class="form-group dviese">
                    <label class="control-label centered">Gimimo metai</label>
                    <div class='ctrl'>
                      <div class='ctrl-button ctrl-button-decrement veins'>-</div>
                      <div class='ctrl-counter do'>
                        <input  name = "gimimo" class='ctrl-counter-input ceturi' maxlength='4' type='text' value='1990'>
                        <div class='ctrl-counter-num tris'>1990</div>
                      </div>
                      <div class='ctrl-button ctrl-button-increment peenki'>+</div>
                    </div>
                  </div>
                  <button class="btn prevBtn btn-lg pull-left" type="button" >Atgal</button>
                  <button class="btn nextBtn btn-lg pull-right" type="button" >Toliau</button>
              </div>
          </div>
      </div>
      <div class="row setup-content" id="step-4">
          <div class="col-xs-12">
              <div class="col-md-12">
                  <h3 style="margin: 10px 0 20px;">Telefono numeris NEBUS viešai matomas, ar naudojamas rinkodaros tikslais</h3>
                  <div class="form-group">
                      <label class="control-label">Jūsų Telefono nr.</label>
                      <input  name = "telefonas" maxlength="9" type="text" required="required" class="form-control telefonas" placeholder="86XXXXXXX" />
                  </div>
                  <h3 style="margin: 10px 0 20px;">Išsaugoję duomenis ir perkrovę naršyklę matysite save komandos narių sąraše!</h3>
                  <button class="btn  prevBtn btn-lg pull-left" type="button" >Atgal</button>
                  <a href='http://localhost/kta.lt/naujaskta/pages/komandos/komandos.php?hello=true'><button id="baigti"class="btn nextBtn btn-lg pull-right" type="submit">Išsaugoti</button></a>
                  
              </div>
          </div>
      </div>
    </form>
  </div>
</div>

<section class="third">
    <wrapper>
       <div class="row">
            <a class="onethird" href="../lenteles/lenteles.php">
                <img src="../../images/lent.png">
                <h2>TURNYRINĖS LENTELĖS</h2>
                <h3>A, B, Veteranų ir D lygos</h3>
                <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> ŽIURĖTI</h4>
            </a>
            <a class="onethird" href="../nuostatai/nuostatai.php">
                <img src="../../images/file.png">
                <h2>PIRMENYBIŲ NUOSTATAI</h2>
                <h3>Oficiali vykdymo tvarka</h3>
                <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> SKAITYTI</h4>
            </a>
            <a class="onethird" href="../sezonai/sezonai.php">
                <img src="../../images/history.png">
                <h2>ANKSTESNI SEZONAI</h2>
                <h3>2012-2018 metų rezultatai</h3>
                <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> PRISIMINTI</h4>
            </a>
        </div>
    </wrapper>
</section>
<section class="forth">
    <div class="container"> 
    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="../akademija/akademija.php">
            <div class="cover">
                <img src="../../images/2.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="../akademija/akademija.php">
            <div class="info">
            <h1>Tinklinio akademija</h1>
            <p>Klaipėdos tinklinio asociacija 2014 m. įkūrė Klaipėdos Tinklinio Akademiją, kurioje trys treneriai augina jaunąją tinklininkų kartą Klaipėdoje.</p>
            </div>
        </a>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="../turnyrai/turnyrai.php">
            <div class="cover">
                <img src="../../images/4.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="../turnyrai/turnyrai.php">
            <div class="info">
            <h1>Tinklinio turnyrai</h1>
            <p>Klaipėdos miesto tinklinio pirmenybės nors ir didžiausias ir svarbiausias tinklinio renginys mieste, tačiau tikrai ne vienintelis. Daugiau kokie turnyrai vyksta mieste...</p>
            </div>
        </a>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="../nuorodos/nuorodos.php">
            <div class="cover">
                <img src="../../images/6.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="../nuorodos/nuorodos.php">
            <div class="info">
            <h1>Naudingos nuorodos</h1>
            <p>Lietuvos tinklinio federacijos ir kitų tinklinio organizacijų internetiniai puslapiai, oficialios taisyklės, kita naudinga literatūra.</p>

            </div>
        </a>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="../treniruotes/treniruotes.php">
            <div class="cover">
                <img src="../../images/3.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="../treniruotes/treniruotes.php">
            <div class="info">
            <h1>Suaugusiujų treniruotės</h1>
            <p>Kviečiame į vakarais darbo dienomis vykstančias suaugusiujų treniruotes, tiek pradedančiujų, tiek pažengusių grupės, moterų ir vyrų..</p>

            </div>
        </a>
    </div>
</div>
    
</section>
<section class="fifth">
    <wrapper>
        <h1>APIE TINKLINIO PIRMENYBES:</h1>
        <h2>Klaipėdos tinklinio asociacija nuo 2012 m. organizuoja svarbiausią metų renginį - Klaipėdos miesto tinklinio pirmenybes!</h2>
        <p>Pirmenybių tikslas propaguoti tinklinį, kaip aktyvų laisvalaikio praleidimo būdą, suburti visus tinklinio mylėtojus į vieną draugišką bendruomenę. Pirmenybės apima visas amžiaus, lyties, ir pajėgumo grupes, nuo 2015 m. pirmenybės vykdomos keturiose lygose: A, B, Veteranų ir Dailiojoje (moterų). Dalyvių geografija apima netik Klaipėdos miestą, sulaukiame dalyvių ir iš Gargždų, Palangos, Plungės, Tauragės, Pajūrio (Šilalės r.). Daugiau nei 6 mėn trunkančiose Klaipėdos miesto tinklinio priemnybėse tinklininkai buriasi į komandas, treniruojasi, tam kad savaitgaliais galėtų pajusti pergalės skonį!</p>
        <div class="butonai">
            <a href="../nuostatai/nuostatai.php"><div class="buttons"> Nuostatai</div></a>
            <a href="../komandos/komandos.php"><div class="buttons"> Komandos</div></a>
            <a href="../kontaktai/kontaktai.php"><div class="buttons"> Kontaktai</div></a>
        </div>
    </wrapper>
</section>
<section class="six">
    <div class="container">
        <div class="col-md-6 col-xs-12">
            <a href="#"><h1>Paplūdimio tinklinis</h1></a>

            <img src="../../images/5.jpg">
            <div class="info">
                <h2>Klaipėdos pliažo tinklinio klubas</h2>
                <p>Ne pelno siekianti organizacija vienijanti paplūdimio tinklinio mylėtojus Klaipėdos mieste. Visa info internetiniame tinklapyje tinklinioarena.lt </p>
                <a href="http://www.tinklinioarena.lt" target="_blank"><div class="buttons">www.tinklinioarena.lt</div></a>
            </div>
        </div>
        <div class="col-md-6 col-xs-12" style="">
            <h1>Pirmenybių video</h1>
            <div class="videoWrapper">
            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FKlaipedostinklinis%2Fvideos%2F1586257588085585%2F&show_text=0&width=560" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div>
        </div>
    </div>
</section>
<section class="remejai">
<div class= "row3">
    <div class="owl-two owl-carousel">
      <div><a target="_blank" href="http://www.feliuga.lt/"><img src = ../../images/remejai/1.png></a></div>
      <div><a target="_blank" href="http://www.klaipeda.lt/"><img src = ../../images/remejai/2.jpg></a></div>
      <div><a target="_blank" href="http://www.gitana.lt/"><img src = ../../images/remejai/3.png></a></div>
      <div><a target="_blank" href="http://www.amberqueen.lt/"><img src = ../../images/remejai/4.png></a></div>
      <div><a target="_blank" href="http://www.klaipedosbaldai.lt/"><img src = ../../images/remejai/5.png></a></div>
      <div><a target="_blank" href="http://www.bs-chemical.lt/"><img src = ../../images/remejai/6.jpg></a></div>
      <div><a target="_blank" href="http://www.4corners.lt/"><img src = ../../images/remejai/7.png></a></div>
      <div><a target="_blank" href="http://www.akvile.lt/"><img src = ../../images/remejai/8.png></a></div>
      <div><a target="_blank" href="http://www.monton.lt/"><img src = ../../images/remejai/9.png></a></div>
    </div>
</div>

</section>
<footer>
    <wrapper>
        <nav>
            <ul>
                <li><a href="../kasnaujo/kasnaujo.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Naujienos</a></li>
                <li><a href="../akademija/akademija.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Akademija</a></li>
                <li><a href="../treniruotes/treniruotes.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Treneriai</a></li>
                <li><a href="../treniruotes/treniruotes.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Treniruotės</a></li>
                <li><a href="../tvarkarastis/tvarkarastis.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Pirmenybės</a></li>
                <li><a href="../rezultatai/rezultatai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Rezultatai</a></li>
                <li><a href="../tvarkarastis/tvarkarastis.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Tvarkaraštis</a></li>
                <li><a href="../lenteles/lenteles.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Lentelės</a></li>
                <li><a href="../komandos/komandos.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Komandos</a></li>
                <li><a href="../nuostatai/nuostatai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Nuostatai</a></li>
                <li><a href="../sezonai/sezonai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Praėję sezonai</a></li>
                <li><a href="../turnyrai/turnyrai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Turnyrai</a></li>
                <li><a href="../amberqueen/amberqueen.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Amber Queen</a></li>
                <li><a href="../nuorodos/nuorodos.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Nuorodos</a></li>
                <li><a href="http://www.tinklinioarena.lt"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Paplūdimio tinklinis</a></li>
                <li><a href="../kontaktai/kontaktai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Susisiekite</a></li>
                <li><a href="https://www.facebook.com/Klaipedostinklinis/"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Mes Facebook'e</a></li>
            </ul>
        </nav>
        <div class="informacija">
            <div class="rekvizitai">
              <h3>Klaipėdos Tinklinio Asociacija<br></h3>
              <h4>Paramos gavėjo kodas: 302843365<br>Vakario g. 15, Ginduliai, Klaipėdos r.<br>Atsiskaitomoji sąskaita: LT107044060007892226, AB SEB Bankas</h4>
            </div>
            <a href="tel:+37069661004"><i class="fa fa-phone-square" aria-hidden="true"></i>  +370 69 66 1004</a>
            <a href="mailto:info@kta.lt"><i class="fa fa-envelope" aria-hidden="true"></i>  info@kta.lt</a>
        </div>
    </wrapper>
</footer>


    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/slider.js"></script>
    <script src="../../js/hamburger.js"></script>
    <script src="../../js/owl.carousel.js"></script>
    <script src="js/komandos.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/inc4.js"></script>
    <script src="js/inc3.js"></script>
    <script src="js/inc2.js"></script>
    <script src="js/inc1.js"></script>
    

    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.2/js/jquery.tablesorter.min.js">

</script>
    <script>
    $(document).ready(function() 
        { 
            $(".table").tablesorter(); 
        } 
    );
    </script>
<?php
  function runMyFunction() {

$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
$conn->set_charset("utf8");
if($conn->connect_error) {
  echo 'Klaida: $conn->connect_error';
} else {
  // echo 'Boooom';
}

$komanda = $_GET['komanda'];
$numeris = $_GET['numeris'];
$vardas = $_GET['vardas'];
$pavarde = $_GET['pavarde'];
$sezonai = $_GET['sezonai'];
$gimimo = $_GET['gimimo'];
$ankstesne = $_GET['ankstesne'];
$ugis = $_GET['ugis'];
$telefonas = $_GET['telefonas'];
$lyga = $_GET['radio'];


$sql = "INSERT INTO zaidejai SET
komanda='$komanda', numeris='$numeris', vardas='$vardas', pavarde='$pavarde',  sezonai='$sezonai', gimimo='$gimimo', ankstesne='$ankstesne', ugis='$ugis', telefonas='$telefonas', lyga='$lyga'";
if ($conn->query ($sql)===TRUE)
 {}else{
  echo $conn->error;
 }
 header("Location:http://www.kta.lt/pages/komandos/komandos.php");
  }

  if (isset($_GET['telefonas'])) {
    runMyFunction();
  }
?>
</body>
</html>