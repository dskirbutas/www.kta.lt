console.log ("veikia");

$(document).ready(function() {
	console.log("vistiek veikia");
});

$(function() {
	console.log("atkaklus gyvate");
});

// ====================KOMANDOS=====================

$('.owl-one').owlCarousel({
    loop:true,
    autoHeight:false, //======================Pakeisti====================
    margin:50,
    nav:true,
    autoplay:false,
    autoplayTimeout:99000,
    navText: ["atgal","toliau"],
    responsive:{
        0:{
            items:1
        },
        800:{
            items:1
        },
        1200:{
            items:2
        }
    }
});

// =====================REMEJAI================

var owl = $('.owl-two');
owl.owlCarousel({
    items:4,
    loop:true,
    // margin:10,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:false,
    smartSpeed: 1000,
    responsiveClass:true,
    responsive:{
        0:{items:1,},
        550:{items:2,},
        760:{items:3,},
        1200:{items:4,},
        1500:{items:5,}
    }
});




$('.owl-carousel').on('mouseenter',function(e){
$(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
})

$('.owl-carousel').on('mouseleave',function(e){
$(this).closest('.owl-carousel').trigger('play.owl.autoplay');
})

// ====================MODAL BOX========================

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// ====================INCREMENT=====================

  $('body').on('keyup', '.numeris', function () {
    var $input = $(this),
        value = $input.val(),
        length = value.length,
        inputCharacter = parseInt(value.slice(-1));

    if (!((length > 1 && inputCharacter >= 0 && inputCharacter <= 9) || (length === 1 && inputCharacter >= 0 && inputCharacter <= 9))) {
        $input.val(value.substring(0, length - 1));
     }
  });

    $('body').on('keyup', '.ketvirtas', function () {
    var $input = $(this),
        value = $input.val(),
        length = value.length,
        inputCharacter = parseInt(value.slice(-1));

    if (!((length > 1 && inputCharacter >= 0 && inputCharacter <= 9) || (length === 1 && inputCharacter >= 0 && inputCharacter <= 9))) {
        $input.val(value.substring(0, length - 1));
     }
  });

    $('body').on('keyup', '.keturi', function () {
    var $input = $(this),
        value = $input.val(),
        length = value.length,
        inputCharacter = parseInt(value.slice(-1));

    if (!((length > 1 && inputCharacter >= 0 && inputCharacter <= 9) || (length === 1 && inputCharacter >= 0 && inputCharacter <= 9))) {
        $input.val(value.substring(0, length - 1));
     }
  });

    $('body').on('keyup', '.ceturi', function () {
    var $input = $(this),
        value = $input.val(),
        length = value.length,
        inputCharacter = parseInt(value.slice(-1));

    if (!((length > 1 && inputCharacter >= 0 && inputCharacter <= 9) || (length === 1 && inputCharacter >= 0 && inputCharacter <= 9))) {
        $input.val(value.substring(0, length - 1));
     }
  });

    $('body').on('keyup', '.telefonas', function () {
    var $input = $(this),
        value = $input.val(),
        length = value.length,
        inputCharacter = parseInt(value.slice(-1));

    if (!((length > 2 && inputCharacter >= 0 && inputCharacter <= 9) || (length === 1 && inputCharacter === 8) || (length === 2 && inputCharacter === 6))) {
        $input.val(value.substring(0, length - 1));
     }
  });

    // =========================AUTO RELOAD=============================


