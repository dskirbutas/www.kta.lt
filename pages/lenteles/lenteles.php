<?php
define('DB_SERVER', 'localhost');
define('DB_USER', 'bschemic_kta');
define('DB_PASS', 'Dainius21');
define('DB_NAME', 'bschemic_kta');


$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
$conn->set_charset("utf8");
if($conn->connect_error) {
  echo 'Klaida: $conn->connect_error';
} 

?>


<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pirmenybių turnyrinė lentelė</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<?php  include '../header.php'; ?> <!-- =====================header====================== -->

<section class="second">
<div class=" owl-one owl-carousel owl-theme" id="A">

  <div class="table-responsive">
    <h1>A LYGA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <?php
          $komandos = array("All Blacks", "Amber Queen", "Gitana", "Klaipėdos Baldai", "KU", "Pajūris", "Vakaris");
          echo "<th style=min-width:155px;background-color:#ff5500;border-bottom:2px;border-style:solid;border-color:#ff5500;border-left:none;>"."A"."</th>";
          foreach ($komandos as &$value) {
            echo "<th colspan=\"2\" width=12.4%>".$value."</th>"; //----------prirašyti width vienodą-----------------
          }
          ?>
          <th style="background-color: rgba(255,255,255,0.2);">Žaista</th>
          <th style="background-color: rgba(255,255,255,0.2);">Koef.</th>
          <th style="background-color: rgba(255,255,255,0.2);">Taškai</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
for($s=0;$s<sizeof($komandos);$s++){
  $sql = "SELECT league, team1, team2, score FROM tvarkarastis WHERE league = 'A' AND team2 = '$komandos[$s]' OR league = 'A' AND team1 = '$komandos[$s]' ORDER BY date ASC";
  $result = $conn->query($sql) or exit(); 

  while ($array = $result->fetch_assoc()) {
    $masyvas[] = $array;}

  echo "<tr>";
  echo "<td rowspan=\"2\" style=background-color:rgba(255,255,255,0.4);border-color:rgba(255,255,255,0);border-bottom-color:rgba(0,0,0,0.1);font-weight:600;text-align:left;>".$komandos[$s]."</td>";
  $p=0;
  $k1=array();
  $k2=array();
  $t=array();

// =============================Taškų skaičiavimas===================================

  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "3:0"){
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "3:1") {
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "3:2") {
            array_push($t, 2);
          }
          elseif ($varzybos['score'] === "2:3") {
            array_push($t, 1);
          }
          elseif ($varzybos['score'] === "1:3") {
            array_push($t, 0);
          }
          elseif ($varzybos['score'] === "0:3") {
            array_push($t, 0);
            }
          elseif ($varzybos['score'] === "") {
            array_push($t, 0);
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "3:0"){
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "3:1") {
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "3:2") {
            array_push($t, 2);
          }
          elseif (strrev($varzybos['score']) === "2:3") {
            array_push($t, 1);
          }
          elseif (strrev($varzybos['score']) === "1:3") {
            array_push($t, 0);
          }
          elseif (strrev($varzybos['score']) === "0:3") {
            array_push($t, 0);
          } 
            elseif (strrev($varzybos['score']) === "") {
            array_push($t, 0);
          }
        }
      }
    }
  }

// =============================Taškų skaičiavimas baigtas===================================


  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          echo "<td>".$varzybos['score']."</td>";
          $prap=(int)substr($varzybos['score'], -1);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -3);
          array_push($k1, $laim); //----------kiek laimeta-----------------
          
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          echo "<td>".strrev($varzybos['score'])."</td>";
          $prap=(int)substr($varzybos['score'], -3);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -1);
          array_push($k1, $laim); //----------kiek laimeta-----------------
        }
      }
    }
    foreach ($masyvas as $varzybos) {
      if($varzybos['score'] === ""){
        $p++;
      }
    }
  }
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".(sizeof($komandos)*2-2-$p/sizeof($komandos))."</td>"; //----------(7*2-2)-$p/7-----------------
    if (array_sum($k2) === 0) {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">"."MAX"."</td>";
    } else {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".round(array_sum($k1)/array_sum($k2),2)."</td>"; 
    };
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".array_sum($t)."</td>"; 
  echo "</tr>";
  echo "<tr>";
  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "3:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "3:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "3:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif ($varzybos['score'] === "2:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif ($varzybos['score'] === "1:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          }
          elseif ($varzybos['score'] === "0:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
            }
          elseif ($varzybos['score'] === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "3:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "3:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "3:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif (strrev($varzybos['score']) === "2:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif (strrev($varzybos['score']) === "1:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          }
          elseif (strrev($varzybos['score']) === "0:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          } 
            elseif (strrev($varzybos['score']) === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
      }
    }
  }
  echo "</tr>";
  unset($masyvas);
}
?>
      </tbody>
    </table>
  </div>


  <div class="table-responsive">
    <h1>B LYGA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <?php
          $komandos = array("Grensena", "Oniks", "VT Klaipėda", "Giruliai", "Šilas-SM", "KTA");
          echo "<th style=min-width:155px;background-color:#ff5500;border-bottom:2px;border-style:solid;border-color:#ff5500;border-left:none;>"."B"."</th>";
          foreach ($komandos as &$value) {
            echo "<th colspan=\"2\" width=12.4%>".$value."</th>"; //----------prirašyti width vienodą-----------------
          }
          ?>
          <th style="background-color: rgba(255,255,255,0.2);">Žaista</th>
          <th style="background-color: rgba(255,255,255,0.2);">Koef.</th>
          <th style="background-color: rgba(255,255,255,0.2);">Taškai</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
for($s=0;$s<sizeof($komandos);$s++){
  $sql = "SELECT team1, team2, score FROM tvarkarastis WHERE league = 'B' AND team2 = '$komandos[$s]' OR league = 'B' AND team1 = '$komandos[$s]' ORDER BY date ASC";
  $result = $conn->query($sql) or exit(); 

  while ($array = $result->fetch_assoc()) {
    $masyvas[] = $array;}

  echo "<tr>";
  echo "<td rowspan=\"2\" style=background-color:rgba(255,255,255,0.4);border-color:rgba(255,255,255,0);border-bottom-color:rgba(0,0,0,0.1);font-weight:600;text-align:left;>".$komandos[$s]."</td>";
  $p=0;
  $k1=array();
  $k2=array();
  $t=array();

// =============================Taškų skaičiavimas===================================

  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "3:0"){
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "3:1") {
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "3:2") {
            array_push($t, 2);
          }
          elseif ($varzybos['score'] === "2:3") {
            array_push($t, 1);
          }
          elseif ($varzybos['score'] === "1:3") {
            array_push($t, 0);
          }
          elseif ($varzybos['score'] === "0:3") {
            array_push($t, 0);
            }
          elseif ($varzybos['score'] === "") {
            array_push($t, 0);
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "3:0"){
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "3:1") {
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "3:2") {
            array_push($t, 2);
          }
          elseif (strrev($varzybos['score']) === "2:3") {
            array_push($t, 1);
          }
          elseif (strrev($varzybos['score']) === "1:3") {
            array_push($t, 0);
          }
          elseif (strrev($varzybos['score']) === "0:3") {
            array_push($t, 0);
          } 
            elseif (strrev($varzybos['score']) === "") {
            array_push($t, 0);
          }
        }
      }
    }
  }

// =============================Taškų skaičiavimas baigtas===================================


  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          echo "<td>".$varzybos['score']."</td>";
          $prap=(int)substr($varzybos['score'], -1);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -3);
          array_push($k1, $laim); //----------kiek laimeta-----------------
          
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          echo "<td>".strrev($varzybos['score'])."</td>";
          $prap=(int)substr($varzybos['score'], -3);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -1);
          array_push($k1, $laim); //----------kiek laimeta-----------------
        }
      }
    }
    foreach ($masyvas as $varzybos) {
      if($varzybos['score'] === ""){
        $p++;
    }
  }
  }
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".(sizeof($komandos)*2-2-$p/sizeof($komandos))."</td>"; //----------(7*2-2)-$p/7-----------------
    if (array_sum($k2) === 0) {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">"."MAX"."</td>";
    } else {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".round(array_sum($k1)/array_sum($k2),2)."</td>"; 
    };
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".array_sum($t)."</td>"; 
  echo "</tr>";
  echo "<tr>";
  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "3:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "3:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "3:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif ($varzybos['score'] === "2:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif ($varzybos['score'] === "1:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          }
          elseif ($varzybos['score'] === "0:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
            }
          elseif ($varzybos['score'] === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "3:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "3:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "3:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif (strrev($varzybos['score']) === "2:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif (strrev($varzybos['score']) === "1:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          }
          elseif (strrev($varzybos['score']) === "0:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          } 
            elseif (strrev($varzybos['score']) === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
      }
    }
  }
  echo "</tr>";
  unset($masyvas);
}
?>
      </tbody>
    </table>
  </div>

<div class="table-responsive"> <!--===================================VETERANAI================ -->
    <h1>VETERANŲ LYGA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <?php
          $komandos = array("Gitana", "Vakaris", "Morgan", "Giruliai", "Plungė");
          echo "<th style=min-width:155px;background-color:#ff5500;border-bottom:2px;border-style:solid;border-color:#ff5500;border-left:none;>"."V"."</th>";
          foreach ($komandos as &$value) {
            echo "<th width=12.4%>".$value."</th>"; //----------prirašyti width vienodą-----------------
          }
          ?>
          <th style="background-color: rgba(255,255,255,0.2);">Žaista</th>
          <th style="background-color: rgba(255,255,255,0.2);">Koef.</th>
          <th style="background-color: rgba(255,255,255,0.2);">Taškai</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
for($s=0;$s<sizeof($komandos);$s++){
  $sql = "SELECT league, team1, team2, score FROM tvarkarastis WHERE league = 'V' AND team2 = '$komandos[$s]' OR league = 'V' AND team1 = '$komandos[$s]' ORDER BY date ASC";
  $result = $conn->query($sql) or exit(); 

  while ($array = $result->fetch_assoc()) {
    $masyvas[] = $array;}

  echo "<tr>";
  echo "<td rowspan=\"2\" style=background-color:rgba(255,255,255,0.4);border-color:rgba(255,255,255,0);border-bottom-color:rgba(0,0,0,0.1);font-weight:600;text-align:left;>".$komandos[$s]."</td>";
  $p=0;
  $k1=array();
  $k2=array();
  $t=array();

// =============================Taškų skaičiavimas===================================

  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "3:0"){
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "3:1") {
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "3:2") {
            array_push($t, 2);
          }
          elseif ($varzybos['score'] === "2:3") {
            array_push($t, 1);
          }
          elseif ($varzybos['score'] === "1:3") {
            array_push($t, 0);
          }
          elseif ($varzybos['score'] === "0:3") {
            array_push($t, 0);
            }
          elseif ($varzybos['score'] === "") {
            array_push($t, 0);
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "3:0"){
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "3:1") {
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "3:2") {
            array_push($t, 2);
          }
          elseif (strrev($varzybos['score']) === "2:3") {
            array_push($t, 1);
          }
          elseif (strrev($varzybos['score']) === "1:3") {
            array_push($t, 0);
          }
          elseif (strrev($varzybos['score']) === "0:3") {
            array_push($t, 0);
          } 
            elseif (strrev($varzybos['score']) === "") {
            array_push($t, 0);
          }
        }
      }
    }
  }

// =============================Taškų skaičiavimas baigtas===================================


  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          echo "<td>".$varzybos['score']."</td>";
          $prap=(int)substr($varzybos['score'], -1);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -3);
          array_push($k1, $laim); //----------kiek laimeta-----------------
          
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          echo "<td>".strrev($varzybos['score'])."</td>";
          $prap=(int)substr($varzybos['score'], -3);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -1);
          array_push($k1, $laim); //----------kiek laimeta-----------------
        }
      }
    }
    foreach ($masyvas as $varzybos) {
      if($varzybos['score'] === ""){
        $p++;
      }
    }
  }
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".(sizeof($komandos)-1-$p/sizeof($komandos))."</td>"; //----------(7*2-2)-$p/7-----------------
    if (array_sum($k2) === 0) {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">"."MAX"."</td>";
    } else {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".round(array_sum($k1)/array_sum($k2),2)."</td>"; 
    };
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".array_sum($t)."</td>"; 
  echo "</tr>";

  echo "<tr>";
  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "3:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "3:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "3:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif ($varzybos['score'] === "2:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif ($varzybos['score'] === "1:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          }
          elseif ($varzybos['score'] === "0:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
            }
          elseif ($varzybos['score'] === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "3:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "3:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "3:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif (strrev($varzybos['score']) === "2:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif (strrev($varzybos['score']) === "1:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          }
          elseif (strrev($varzybos['score']) === "0:3") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          } 
            elseif (strrev($varzybos['score']) === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
      }
    }
  }
  echo "</tr>";
  unset($masyvas);
}
?>
      </tbody>
    </table>
  </div>

<div class="table-responsive"> <!--============================-Dailioji lyga==========================-->
    <h1>DAILIOJI LYGA</h1>
    <table class="table">
      <thead class="tableheader">
        <tr>
          <?php
          $komandos = array("Klaipėda", "Gargždų SC", "Gargždų SC mėgėjos", "Viesulas", "Plungės SRC", "KU", "Vakarų tinklinis");
          echo "<th style=min-width:155px;background-color:#ff5500;border-bottom:2px;border-style:solid;border-color:#ff5500;border-left:none;>"."D"."</th>";
          foreach ($komandos as &$value) {
            echo "<th width=12.4%>".$value."</th>"; //----------prirašyti width vienodą-----------------
          }
          ?>
          <th style="background-color: rgba(255,255,255,0.2);">Žaista</th>
          <th style="background-color: rgba(255,255,255,0.2);">Koef.</th>
          <th style="background-color: rgba(255,255,255,0.2);">Taškai</th>
        </tr>
      </thead>
      <tbody class="tablecontent">

<?php
for($s=0;$s<sizeof($komandos);$s++){
  $sql = "SELECT team1, team2, score FROM tvarkarastis WHERE league = 'D' AND team2 = '$komandos[$s]' OR league = 'D' AND team1 = '$komandos[$s]' ORDER BY date ASC";
  $result = $conn->query($sql) or exit(); 
  while ($array = $result->fetch_assoc()) {
    $masyvas[] = $array;}
  echo "<tr>";
  echo "<td rowspan=\"2\" style=background-color:rgba(255,255,255,0.4);border-color:rgba(255,255,255,0);border-bottom-color:rgba(0,0,0,0.1);font-weight:600;text-align:left;>".$komandos[$s]."</td>";
  $p=0;
  $k1=array();
  $k2=array();
  $t=array();

// =============================Taškų skaičiavimas===================================

  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "2:0"){
            array_push($t, 3);
          }
          elseif ($varzybos['score'] === "2:1") {
            array_push($t, 2);
          }
          elseif ($varzybos['score'] === "1:2") {
            array_push($t, 1);
          }
          elseif ($varzybos['score'] === "0:2") {
            array_push($t, 0);
            }
          elseif ($varzybos['score'] === "") {
            array_push($t, 0);
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "2:0"){
            array_push($t, 3);
          }
          elseif (strrev($varzybos['score']) === "2:1") {
            array_push($t, 2);
          }
          elseif (strrev($varzybos['score']) === "1:2") {
            array_push($t, 1);
          }
          elseif (strrev($varzybos['score']) === "0:2") {
            array_push($t, 0);
          } 
            elseif (strrev($varzybos['score']) === "") {
            array_push($t, 0);
          }
        }
      }
    }
  }

// =============================Taškų skaičiavimas baigtas===================================


  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          echo "<td>".$varzybos['score']."</td>";
          $prap=(int)substr($varzybos['score'], -1);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -3);
          array_push($k1, $laim); //----------kiek laimeta-----------------
          
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          echo "<td>".strrev($varzybos['score'])."</td>";
          $prap=(int)substr($varzybos['score'], -3);
          array_push($k2, $prap); //----------kiek prapista-----------------
          $laim=(int)substr($varzybos['score'], -1);
          array_push($k1, $laim); //----------kiek laimeta-----------------
        }
      }
    }
    foreach ($masyvas as $varzybos) {
      if($varzybos['score'] === ""){
        $p++;
    }
  }
  }
  // ---------------kiek sužaista--------------------
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".(sizeof($komandos)-1-$p/sizeof($komandos))."</td>"; //----------(7*2-2)-$p/7-----------------
  // ------------------koeficientas--------------------
    if (array_sum($k2) === 0) {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">"."MAX"."</td>";
    } else {
        echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".round(array_sum($k1)/array_sum($k2),2)."</td>"; 
    };
  // ------------------taškai----------------------------
  echo "<td style=background-color:rgba(255,255,255,0.2);border-bottom-color:rgba(255,255,255,0.3); rowspan=\"2\">".array_sum($t)."</td>"; 
  // -------------------------------------------------------
  echo "</tr>";

  echo "<tr>";
  for ($i=0; $i < sizeof($komandos); $i++) { 
    foreach ($masyvas as $varzybos) {
      if($i === $s){
        echo "<td style=background-color:#00C0C6;border:none;></td>";
        break;
      }
      if($varzybos['team1'] != $varzybos['team2']){
        if($varzybos['team2'] === $komandos[$i]){
          if ($varzybos['score'] === "2:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif ($varzybos['score'] === "2:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif ($varzybos['score'] === "1:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif ($varzybos['score'] === "0:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
            }
          elseif ($varzybos['score'] === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
        elseif($varzybos['team1'] === $komandos[$i]){
          if (strrev($varzybos['score']) === "2:0"){
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."3"."</td>";
          }
          elseif (strrev($varzybos['score']) === "2:1") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."2"."</td>";
          }
          elseif (strrev($varzybos['score']) === "1:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."1"."</td>";
          }
          elseif (strrev($varzybos['score']) === "0:2") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>"."0"."</td>";
          } 
            elseif (strrev($varzybos['score']) === "") {
            echo "<td style=border-bottom-color:rgba(255,255,255,0.3);>".""."</td>";
          }
        }
      }
    }
  }
  echo "</tr>";
  unset($masyvas);
}
?>
      </tbody>
    </table>
  </div>

</div>
</section>

<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.2/js/jquery.tablesorter.min.js">

</script>
    <script>
    $(document).ready(function() 
        { 
            $("#tva").tablesorter(); 
        } 
    );
    </script>

</body>
</html>