

// ====================CAROUSEL=====================

$('.owl-one').owlCarousel({
    loop:true,
    autoHeight:false, //======================Pakeisti====================
    margin:50,
    nav:true,
    autoplay:false,
    autoplayTimeout:99000,
    navText: false,
    navText: ["<i class=\"fas fa-chevron-left\"></i>","<i class=\"fas fa-chevron-right\"></i>"],
    items:1,
    touchDrag: false,
    pullDrag: false,
});

// =====================REMEJAI================

var owl = $('.owl-two');
owl.owlCarousel({
    items:4,
    loop:true,
    // margin:10,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:false,
    smartSpeed: 1000,
    responsiveClass:true,
    responsive:{
        0:{items:1,},
        550:{items:2,},
        760:{items:3,},
        1200:{items:4,},
        1500:{items:5,}
    }
});




$('.owl-carousel').on('mouseenter',function(e){
$(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
})

$('.owl-carousel').on('mouseleave',function(e){
$(this).closest('.owl-carousel').trigger('play.owl.autoplay');
})

