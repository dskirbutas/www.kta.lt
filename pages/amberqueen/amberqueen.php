<html>
<head>
          <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=windows-UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../../favicon.ico" type="image/x-icon">
    <title>Tinklinio komanda Amber Queen</title> 
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<?php  include '../header.php'; ?> <!-- =====================header====================== -->
	<section class="second">
        <wrapper>
            <h1>TK "AMBER QUEEN"</h1>

            <p style=""><img id="aqlogo" src="../../images/amberqueen.png"> &emsp;Klaipėdos tinkliniui 2017-2018 m. sezonas ypatingas, Klaipėdos komanda pagaliau dalyvavo Lietuvos vyrų tinklinio čempionate. Klaipėdos sporto klubo „Pajūrio tinklinis“ suburta aukščiausio lygio komanda „Amber Queen“ džiugino tinklinio mylėtojus pritraukdama į Klaipėdą pasivaržyti geriausias Lietuvos tinklinio komandas.<br>

            O viskas prasidėjo dar praėjusį sezoną, kai tuometinę komandą RELAX pradėjo treniruoti treneris Sergii Shchegol, turintis patirties Ukrainos aukščiausioje lygoje ir jau kelerius metus treniruojantis Klaipėdos tinklininkus. RELAX komandai tapus 2016-2017 m. Klaipėdos miesto čempionais, komandos vadovas Salvijus Paškauskas su treneriu prakalbo apie miesto komandą, kuri žaistų LTF  aukščiausioje lygoje.</p>

            <p>Apsilankius pas Klaipėdos savivaldybės administracijos direktorių ir gavus patvirtinimą, jog miestas pagal galimybes palaikys šį tinklinio projektą, imta ieškoti rėmėjų. RELAX rėmėjas UAB "ARLANGA" ir toliau sutiko remti naująją komandą, prie jų prisijungė ir AB "Klaipėdos Energija". Pagrindinis rėmėjas, davęs komandai vardą, AMBER QUEEN, ir jos vadovas, neabejingas tinkliniui Aleksandras Afanasjevas, suteikė visas sąlygas formuoti komandą Lietuvos aukščiausioje tinklinio lygoje. Prie rėmėjų prisijungė ir AB "Klaipėdos Baldai" bei informaciniai rėmėjai: dienraštis KLAIPĖDA ir radijo stotis LALUNA.</p>

            <p>Vienas svarbiausių atliktų darbų sezono metu - Sporto rūmų salės pritaikymas tinklinio varžyboms. Sporto klubo "Pajūrio tinklinis" vadovo pastangomis buvo nupirktas visas reikalingas inventorius, sužymėtos linijos, įstatytos kapsulės tinklinio stovams tvirtinti. Nuo šiol Klaipėda turės erdvią ir talpią sporto salę aukščiausio lygio tinklinio varžyboms. </p>

            <p><i>"Komandos tikslai šiam sezonui buvo sukomplektuoti miesto komandą ir išsibandyti. Po sezono žiūreti, kaip kas ir tada dėlioti jau realius tikslus kitam sezonui. Realybėje gavosi, kad mes „peršokome“ savo parodytu lygiu visus metus"</i>, - teigė komandos vadovas Salvijus Paškauskas.</p><p>

            <span id="myImg" class="roll"></span>
            <img  id="aqImg" alt="Amber Queen tinklinio komanda" style="" src="../../images/amberis.jpg">

            Sezono pradžioje komandą sudarė klaipėdiečiai, vėliau, spalio mėnesį, iš Ukrainos atvažiavo krašto puolėjas Maksim Turyk bei prisijungė vilnietis Tomas Staševičius. Gruodį teko atsisakyti Maksim Turyk paslaugų. Komandą paliko ir jungiantysis žaidėjas Andrius Vilbikas. Juos sėkmingai pakeitė jungėjas iš Kelmės Marius Barauskis ir vilnietis puolėjas Artūr Vincėlovič.<br><br>

            Prasidėjus čempionatui komanda sparčiai tobulėjo, po pirmojo rato Amber Queen turnyrinėje lentelėje rikiavosi antroje vietoje. Čempionato metu iki atkrintamųjų varžybų komanda pralaimėjo tik 3 varžybas ir pasiekė 13 pergalių. Kovo 11d. Amber Queen komandai čempionatas baigėsi. Atkrintamosiose varžybose jiems teko pripažinti Kelmės "ETOVIS" komandos pranašumą. Amber Queen užėmė 5-ąją vietą iš 10-ties komandų, dalyvavusių Lietuvos vyrų tinklinio čempionate.</p>

            <div id="myModal" class="modal">
                <span class="close">&times;</span>
                <!-- Modal Content (The Image) -->
                <img class="modal-content" id="img01">
            </div>
<hr>
        <h2>"Amber Queen" komandos sudėtis</h2>
        <div class="table-responsive">
            <table class="table" id="table">
              <thead class="tableheader">
                <tr>
                  <th width="5%">Nr.</th>
                  <th width="40%">Vardas, Pavardė</th>
                  <th width="10%">Gimimo metai</th>
                  <th width="10%">Ūgis</th>
                  <th width="10%">Svoris</th>
                  <th width="15%">Pozicija</th>
                </tr>
              </thead>
              <tbody class="tablecontent">

        <tr>
              <td>1</td>
              <td>Sergii Shchegol</td>
              <td>1985</td>
              <td>179</td>
              <td>81</td>
              <td>Libero</td>
        <tr>
        <tr>
              <td>2</td>
              <td>Robertas Ripkauskas</td>
              <td>1989</td>
              <td>180</td>
              <td>80</td>
              <td>Jungiantis/libero</td>
        <tr>
        <tr>
              <td>3</td>
              <td>Vilius Rubavičius</td>
              <td>1994</td>
              <td>194</td>
              <td>95</td>
              <td>Priėmėjas</td>
        <tr>
        <tr>
              <td>4</td>
              <td>Artūr Vincėlovič</td>
              <td>1997</td>
              <td>183</td>
              <td>84</td>
              <td>Priėmėjas</td>
        <tr>
        <tr>
              <td>5</td>
              <td>Rokas Aukštkalnis</td>
              <td>1994</td>
              <td>192</td>
              <td>90</td>
              <td>Priėmėjas</td>
        <tr>
        <tr>
              <td>6</td>
              <td>Albertas Pocius</td>
              <td>2001</td>
              <td>184</td>
              <td>78</td>
              <td>Priėmėjas</td>
        <tr>
        <tr>
              <td>7</td>
              <td>Gintautas Makauskas</td>
              <td>1989</td>
              <td>179</td>
              <td>95</td>
              <td>Jungiantis</td>
        <tr>
        <tr>
              <td>8</td>
              <td>Algirdas Vengraitis</td>
              <td>1990</td>
              <td>186</td>
              <td>95</td>
              <td>Opozitas</td>
        <tr>
        <tr>
              <td>9</td>
              <td>Marius Barauskis</td>
              <td>1999</td>
              <td>192</td>
              <td>84</td>
              <td>Jungiantis</td>
        <tr>
            <tr>
              <td>10</td>
              <td>Evaldas Tamošiūnas</td>
              <td>1995</td>
              <td>183</td>
              <td>84</td>
              <td>Priėmėjas/Opozitas</td>
        <tr>
        <tr>
              <td>11</td>
              <td>Linas Starkutis</td>
              <td>1994</td>
              <td>203</td>
              <td>96</td>
              <td>Centro blokuotojas</td>
        <tr>
        <tr>
              <td>12</td>
              <td>Edvinas Žaltauskas</td>
              <td>1994</td>
              <td>193</td>
              <td>75</td>
              <td>Centro blokuotojas</td>
        <tr>
        <tr>
              <td>14</td>
              <td>Tadas Viliopas</td>
              <td>1999</td>
              <td>173</td>
              <td>75</td>
              <td>Libero</td>
        <tr>
        <tr>
              <td>15</td>
              <td>Martynas Smulkys</td>
              <td>1986</td>
              <td>192</td>
              <td>89</td>
              <td>Centro blokuotojas</td>
        <tr>
        <tr>
              <td>17</td>
              <td>Tomas Staševičius</td>
              <td>1987</td>
              <td>195</td>
              <td>80</td>
              <td>Opozitas</td>
        <tr>
              </tbody>
            </table>
          </div>

    </wrapper>
</section>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->
<script src="modal.js"></script>
</body>
</html>