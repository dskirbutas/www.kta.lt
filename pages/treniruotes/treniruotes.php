<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tinklinio treniruotės</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
<?php  include '../header.php'; ?> <!-- =====================header====================== -->

<section class="second">
    <wrapper>
        <h1 style="margin-bottom: 12px">Suaugusiujų treniruotės</h1>
<!--         <h3 style="padding: 2vh 10vw;">Xxxx xxxxxxxxxxxx x xxxxxxxxxxxxxxx xxxxxxxxxxxxxxxx x xxxxx xxxxxxxxxxxxxxxx x xx xxxxxxxxxxxxxxxxxxxxxx xxxxxxxxxx xxxxxxx xxxxxxx xxxxxxxxxxx xxxxxxxxx x x xxxx x xxxx        x xxxxxxxx x x xxxxxxxxxxxxxxxx x x xxxxxxxxxxxxx  xxxxxxx xxxxxxxx xxxxx xxxxxxxxx xxxxxxx xxxxxxxx xxx x  xxxxxxxx xxxxxxx xxxx xxxxxxxxxxx xxx xxxxxx xxxxxx xxxxx xxxxxxx xxxxx xxxxxxxx xxxxxx xxxxx xxxxxxxx xxx xxxxxx xx xxxxxxx xxxxxx. </h3> -->
        <div class="trninfo">
            <div class="treniruotes">
                <div class="teammember teammember2">
                    <div class="teammember__content">
                        <h2 class="teammember__content__office">VYRAI</h2>
            			<ul class="teammember__content__contact">
            	                <li class="teammember__content__contact__diena"><p>Pirmadieniais<br>Ketvirtadieniais</p></li>
            	                <li class="teammember__content__contact__laikas"><p>20:00-21:40</p></li>
            	                <li class="teammember__content__contact__vieta"><p>H. Zudermano gimnazija<br>Debreceno g. 29</p></li>
            	                <li class="teammember__content__contact__kaina"><p>40 Eur/mėn</p></li>
            	                <li class="teammember__content__contact__coach"><p>Laurynas Gedminas</p></li>
            	        </ul>
                    </div>
                </div>
            </div>
            <div class="treniruotes">
                <div class="teammember teammember2">
                    <div class="teammember__content">
                        <h2 class="teammember__content__merginos">MOTERYS</h2>
            			<ul class="teammember__content__contact">
            	                <li class="teammember__content__contact__diena"><p>Pirmadieniais<br>Trečiadieniais</p></li>
            	                <li class="teammember__content__contact__laikas"><p>18:30-20:00</p></li>
            	                <li class="teammember__content__contact__vieta"><p>KKRC Sporto salė<br>Pilies g. 2A</p></li>
            	                <li class="teammember__content__contact__kaina"><p>40 Eur/mėn</p></li>
            	                <li class="teammember__content__contact__coach"><p>Agnė Aleknavičienė</p></li>
            	        </ul>
                    </div>
                </div>
            </div>
            <div class="treniruotes">
                <div class="teammember teammember2">
                    <div class="teammember__content">
                        <h2 class="teammember__content__merginos">MOTERYS</h2>
            			<ul class="teammember__content__contact">
            	                <li class="teammember__content__contact__diena"><p>Antradieniais<br>Ketvirtadieniais</p></li>
            	                <li class="teammember__content__contact__laikas"><p>17:30-19:00</p></li>
            	                <li class="teammember__content__contact__vieta"><p>Sendvario gimnazija<br>Tilžės g. 39</p></li>
            	                <li class="teammember__content__contact__kaina"><p>35 Eur/mėn</p></li>
            	                <li class="teammember__content__contact__coach"><p>Laurynas Gedminas</p></li>
            	        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="treneriai">
            <div class="inline">
        	    <div class="teammember" style="margin-bottom: 10px;">
        	        <div class="teammember__img" title="Image of Dainius Blazevicius"></div>
        	        <div class="teammember__content">
        	            <h2 class="teammember__content__name"> Agnė Aleknavičienė</h2>
        	            <h3 class="teammember__content__job"> Trenerė</h3>
        	            <ul class="teammember__content__contact">
        	                <li class="teammember__content__contact__phone"><a href="tel:+37069810140">+370 609 49647</a></li>
        	                <li class="teammember__content__contact__email"><a href="mailto:tinklinioasociacija@gmail.com">a.aleknaviciene@yahoo.com</a></li>
        	                <li class="teammember__content__contact__slack"><a target="_blank" href="https://www.facebook.com/a.aleknaviciene">a.aleknaviciene</a></li>
        	            </ul>
        	        </div>
        	    </div>
            </div>
            <div class="inline">
        	    <div class="teammember" style="margin-bottom: 10px;" >
        	        <div style="background:url( '../../images/laurynas.jpg'); background-size: cover;" class="teammember__img" title="Image of Skirbutas">
        	        </div>
        	        <div class="teammember__content">
        	            <h2  class="teammember__content__name"> Laurynas Gedminas
        	            </h2>
        	            <h3 class="teammember__content__job"> Treneris
        	            </h3>
        	            <ul class="teammember__content__contact">
        	                <li class="teammember__content__contact__phone"><a href="tel:+37069810140">+37062 920 749</a></li>
        	                <li class="teammember__content__contact__email"><a href="mailto:tinklinioasociacija@gmail.com">laurynasgedminas94@gmail.com</a></li>
        	                <li class="teammember__content__contact__slack"><a target="_blank" href="https://www.facebook.com/laurynas.gedminas.7">laurynas.gedminas.7</a></li>
        	            </ul>
        	        </div>
        	    </div>
            </div>
        </div>





	</wrapper>
</section>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

</body>
</html>