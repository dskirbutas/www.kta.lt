<?php
define('DB_SERVER', 'localhost');
define('DB_USER', 'bschemic_kta');
define('DB_PASS', 'Dainius21');
define('DB_NAME', 'bschemic_kta');


$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
$conn->set_charset("utf8");
if($conn->connect_error) {
  echo 'Klaida: $conn->connect_error';
} else {
  // echo 'Boooom';
}

  $sql = 'SELECT * FROM tvarkarastis ORDER BY date, time ASC';


  $result = $conn->query($sql); 

?>


<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pirmenybių tvarkaraštis</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
      <?php  include '../header.php'; ?> <!-- =====================header====================== -->

<section class="second">
    <div id="A">
      <!--for demo wrap-->
      <h1>Tvarkaraštis</h1>
    <div class="form-group">
      <div class="radio-tile-group">
        <div class="input-container">
          <input id="walk" class="radio-button" type="radio" name="radio" value="A" />
          <div class="radio-tile">
            <h1>A</h1>
          </div>
        </div>
        <div class="input-container">
            <input id="bike" class="radio-button" type="radio" name="radio" value="B" />
            <div class="radio-tile">
              <h1>B</h1>
            </div>
        </div>
        <div class="input-container">
            <input id="drive" class="radio-button" type="radio" name="radio" value="V" />
            <div class="radio-tile">
              <h1>V</h1>
            </div>
        </div>
        <div class="input-container">
            <input id="fly" class="radio-button" type="radio" name="radio" value="D" />
            <div class="radio-tile">
              <h1>D</h1>
            </div>
          </div>
      </div>
  </div>
  <div class="form-group">
    <select id="komandos" class="form-control">
        <option>Rodyti visus</option>
        <option>Amber Queen</option>
         <option>Gitana</option>
        <option value="KU">Klaipėdos Universitetas</option>
        <option>Vakaris</option>
        <option>Klaipėdos Baldai</option>
        <option>Pajūris</option>
        <option>All Blacks</option>
        <option>Šilas-SM</option>
        <option>Grensena</option>
        <option>Giruliai</option>
        <option>KTA</option>
        <option>VT Klaipėda</option>
        <option>Oniks</option>
        <option>Plungė</option>
        <option>Morgan</option>
    </select>
  </div>
      <div class="table-responsive">
        <table class="table" id="table">
          <thead class="tableheader">
            <tr>
              <th width="18%">Data</th>
              <th width="18%">Laikas</th>
              <th style = width="54%">Susitinka</th>
              <th style = "cursor: pointer;" width="10%">Lyga</th>
            </tr>
          </thead>
          <tbody class="tablecontent">
            <?php
     while ($array = $result->fetch_assoc()) {
        if( $array['score'] ===""){
        echo "<tr>";
          echo "<td>".$array['date']."</td>";
          echo "<td>".substr($array['time'], 0,5 )."</td>";
          echo "<td>".$array['team1']." - ". $array['team2']."</td>";
          echo "<td>".$array['league']."</td>";
        echo "</tr>";
      }
    }

    ?>
          </tbody>
        </table>
      </div>
    </div>
</section>

    <script src="filtras.js"></script>

    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.29.2/js/jquery.tablesorter.min.js">

</script>
    <script>
    $(document).ready(function() 
        { 
            $("#table").tablesorter(); 
        } 
    );
    </script>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

</body>
</html>