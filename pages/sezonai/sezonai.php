<!DOCTYPE html>
<html>
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57748026-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57748026-1');
    </script>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Ankstesni sezonai</title>
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    
</head>
<body>
<?php  include '../header.php'; ?> <!-- =====================header====================== -->

	<section class="second">
        <wrapper>
            <h1>Ankstesnių sezonų nugalėtojai</h1>
            <div class="container">
            <section>
              <h4 class="active">2017-2018 m.</h4>
            <ul>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                      <div class="content">
                        <h3>A Lyga</h3><p>
                    1. Amber Queen<br /> 
                    2. KU  <br/> 
                    3. Gitana  <br /> 
                    4. Vakaris <br /> 
                    5. Pajūris  <br /> 
                    6. Klaipėdos Baldai<br /> 
                    7. All Blacks<br/></p>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                      <div class="content">
                        <h3>B Lyga</h3>
                        <p>
                    1. Šilas-SM<br /> 
                    2. Judrėnų Grensena<br/> 
                    3. Giruliai<br /> 
                    4. VT Klaipėda<br /> 
                    5. KTA<br /> 
                    6. Oniks<br /> </p>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                      <div class="content">
                        <h3>V Lyga</h3><p>
                    1. Vakaris<br /> 
                    2. Gitana<br/> 
                    3. Plungė<br /> 
                    4. Giruliai<br /> 
                    5. Morgan<br /> </p>
                      </div>
                  </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                      <div class="content">
                        <h3>D Lyga</h3><p>
                    1. Viesulas<br /> 
                    2. Gargždų SC<br/> 
                    3. Plungės SRC<br /> 
                    4. Klaipėda<br /> 
                    5. Gardždų SC mėgėjos<br /> 
                    6. KU<br /> 
                    7. Vakarų Tinklinis<br/></p>
                      </div>
                    </div>
                  </div>
            </ul>
                <h4>2016-2017 m.</h4>
                <ul>
              <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>A Lyga</h3><p>
                    1. Relax<br /> 
                    2. KU  <br/> 
                    3. Vakaris  <br /> 
                    4. Gitana     <br /> 
                    5. Ciongs  <br /> 
                    6. Klaipėdos Baldai<br /> 
                    7. Akvilė-KBC   <br/></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>B Lyga</h3><p>
                    1. Pajūris  <br /> 
                    2. Šilas-SM  <br /> 
                    3. Viesulas  <br/>
                    4. Maccabi  <br/>         
                    5. Spaikas  <br/>
                    <br>
                    <br>
                    <br></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>V Lyga</h3><p>
                    1. Vakaris  <br /> 
                    2. Gitana  <br /> 3. Morgan  <br /> 
                    4. Versmė  <br /> 
                    5. Giruliai<br />
                    6. Akvilė-Klaipėdos Baldai<br /></p>
                  </div>
              </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>D Lyga</h3><p>
                    1. Gargždų SC  <br /> 
                    2. SK Viesulas  <br /> 
                    3. KTA <br /> 
                    4. KU  <br /> 
                    5. KVK<br />
                    6. Versmė-SRC<br /></p>
                  </div>
                </div>
              </div>

                </ul>
                <h4>2015-2016 m.</h4>
                <ul>
                  <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>A Lyga</h3><p>
                    1. Omega<br /> 
                    2. KU  <br/> 
                    3. Gitana  <br /> 
                    4. Vakaris     <br /> 
                    5. Relax  <br /> 
                    6. Klaipėdos Baldai<br /> 
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>B Lyga</h3><p>
                    1. Giruliai  <br /> 
                    2. Viesulas  <br /> 
                    3. Spaikas  <br/>
                    4. Oxygen  <br/>         
                    5. KVK  <br/>
                    5. Akvilė-KBC  <br/></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                  </div>
              </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                  </div>
                </div>
              </div>
                </ul>
                <h4>2014-2015 m.</h4>
                <ul>
                  <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>A Lyga</h3><p>
                    1. Omega<br /> 
                    2. KU  <br/> 
                    3. Kečas - Gargždų geležinkelis  <br /> 
                    4. Gitana     <br /> 
                    5. Vakaris  <br /> 
                    6. Giruliai<br /> 
                    7. Stihl   <br/></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>B Lyga</h3><p>
                    1. Klaipėdos Baldai  <br /> 
                    2. Relax  <br /> 
                    3. Akvilė-KBC  <br/>
                    4. Dauparai  <br/>         
                    5. Spaikas  <br/>
                    6. Viesulas <br/>
                    <br>
                    <br></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>D Lyga</h3><p>
                    1. Gargždų SC  <br /> 
                    2. Viesulas  <br />
                    3. KU  <br /> 
                    4. Viesulo SC  <br /> 
                  </div>
              </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                  </div>
                </div>
              </div>
                </ul>
                <h4>2013-2014 m.</h4>
                <ul>
                 <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>A Lyga</h3><p>
                    1. Omega<br /> 
                    2. KU  <br/> 
                    3. Vakaris  <br /> 
                    4. Vienminčiai     <br /> 
                    5. Giruliai  <br /> 
                    6. Stihl<br /> 
                    7. Krantas   <br/></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>B Lyga</h3><p>
                    1. Gitana  <br /> 
                    2. KU  <br /> 
                    3. Klaipėdos Baldai  <br/>
                    4. Oxygen  <br/>         
                    5. Dauparai  <br/>
                    6. Maximum  <br/>
                    7. Spaikas  <br/>
                    <br></p>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                    <h3>D Lyga</h3><p>
                    1. SK Viesulas  <br /> 
                    2. Garizda  <br /> 
                    3. Viesulo SC 1 <br /> 
                    4. KU  <br /> 
                    5. Viesulo SC 2<br />
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <div class="content">
                  </div>
                </div>
                </div>
              </ul>
            </section>
        </div>
    </wrapper>
</section>



    <script src="js/sezonai.js"></script>
<?php  include '../footer.php'; ?> <!-- =====================footer====================== -->

</body>
</html>