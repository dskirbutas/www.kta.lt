<html>
<head>
</head>
<body>
<section class="third">
    <wrapper>
        <div class="row">
            <a class="onethird" href="pages/lenteles/lenteles.php">
                <img src="images/lent.png">
                <h2>TURNYRINĖS LENTELĖS</h2>
                <h3>A, B, Veteranų ir D lygos</h3>
                <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> ŽIURĖTI</h4>
            </a>
            <a class="onethird" href="pages/nuostatai/nuostatai.php">
                <img src="images/file.png">
                <h2>PIRMENYBIŲ NUOSTATAI</h2>
                <h3>Oficiali vykdymo tvarka</h3>
                <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> SKAITYTI</h4>
            </a>
            <a class="onethird" href="pages/sezonai/sezonai.php">
                <img src="images/history.png">
                <h2>ANKSTESNI SEZONAI</h2>
                <h3>2012-2018 metų rezultatai</h3>
                <h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> PRISIMINTI</h4>
            </a>
        </div>
    </wrapper>
</section>
<section class="forth">
    <div class="container"> 
    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="pages/akademija/akademija.php">
            <div class="cover">
                <img src="images/2.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="pages/akademija/akademija.php">
            <div class="info">
            <h1>Tinklinio akademija</h1>
            <p>Klaipėdos tinklinio asociacija 2014 m. įkūrė Klaipėdos Tinklinio Akademiją, kurioje trys treneriai augina jaunąją tinklininkų kartą Klaipėdoje.</p>
            </div>
        </a>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="pages/turnyrai/turnyrai.php">
            <div class="cover">
                <img src="images/4.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="pages/turnyrai/turnyrai.php">
            <div class="info">
            <h1>Tinklinio turnyrai</h1>
            <p>Klaipėdos miesto tinklinio pirmenybės nors ir didžiausias tinklinio renginys mieste, tačiau tikrai ne vienintelis. Daugiau kokie turnyrai vyksta mieste...</p>
            </div>
        </a>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="pages/nuorodos/nuorodos.php">
            <div class="cover">
                <img src="images/6.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="pages/nuorodos/nuorodos.php">
            <div class="info">
            <h1>Naudingos nuorodos</h1>
            <p>Lietuvos tinklinio federacijos ir kitų tinklinio organizacijų internetiniai puslapiai, oficialios taisyklės, kita naudinga literatūra.</p>
            </div>
        </a>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12 col-12">
        <a class="link" href="pages/treniruotes/treniruotes.php">
            <div class="cover">
                <img src="images/3.jpg">
                <div class="overlay"></div>
            </div>
        </a>
        <a class="textlink" href="pages/treniruotes/treniruotes.php">
            <div class="info">
            <h1>Suaugusiujų treniruotės</h1>
            <p>Kviečiame į vakarais, darbo dienomis vykstančias suaugusiujų treniruotes, tiek pradedančiujų, tiek pažengusių grupės, moterų ir vyrų..</p>
            </div>
        </a>
    </div>
</div>
    
</section>
<section class="fifth">
    <wrapper>
        <h1>APIE TINKLINIO PIRMENYBES:</h1>
        <h2>Klaipėdos tinklinio asociacija nuo 2012 m. organizuoja svarbiausią metų renginį - Klaipėdos miesto tinklinio pirmenybes!</h2>
        <p>Pirmenybių tikslas - propaguoti tinklinį, kaip aktyvų laisvalaikio praleidimo būdą, suburti visus tinklinio mylėtojus į vieną draugišką bendruomenę. Pirmenybės apima visas amžiaus, lyties, ir pajėgumo grupes. Nuo 2015 m. pirmenybės vykdomos keturiose lygose: A, B, Veteranų ir Dailiojoje (moterų). Dalyvių geografija apima ne tik Klaipėdos miestą. Sulaukiame dalyvių ir iš Gargždų, Palangos, Plungės, Tauragės, Pajūrio (Šilalės r.). Daugiau nei 6 mėn trunkančiose Klaipėdos miesto tinklinio pirmenybėse, tinklininkai buriasi į komandas, treniruojasi, kad savaitgaliais galėtų pajusti pergalės skonį!</p>

        <div class="butonai">
            <a href="pages/nuostatai/nuostatai.php"><div class="buttons"> Nuostatai</div></a>
            <a href="pages/komandos/komandos.php"><div class="buttons"> Komandos</div></a>
            <a href="pages/kontaktai/kontaktai.php"><div class="buttons"> Kontaktai</div></a>
        </div>
    </wrapper>
</section>
<section class="six">
    <div class="container">
        <div class="col-md-6 col-xs-12">
            <h1>Paplūdimio tinklinis</h1>
            <img src="images/5.jpg">
            <div class="info">
                <h2>Klaipėdos pliažo tinklinio klubas</h2>
                <p>Ne pelno siekianti organizacija, vienijanti paplūdimio tinklinio mylėtojus Klaipėdos mieste. Visa info internetiniame tinklapyje tinklinioarena.lt</p>
                <a href="http://www.tinklinioarena.lt" target="_blank"><div class="buttons">www.tinklinioarena.lt</div></a>
            </div>
        </div>
        <div class="col-md-6 col-xs-12" style="">
            <h1>Pirmenybių video</h1>
            <div class="videoWrapper">
            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FKlaipedostinklinis%2Fvideos%2F1586257588085585%2F&show_text=0&width=560" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
            </div>
        </div>
    </div>
</section>
<section class="remejai">
<div class= "row3">
    <div class="owl-two owl-carousel">
      <div><a target="_blank" href="http://www.feliuga.lt/"><img src = images/remejai/1.png></a></div>
      <div><a target="_blank" href="http://www.klaipeda.lt/"><img src = images/remejai/2.jpg></a></div>
      <div><a target="_blank" href="http://www.gitana.lt/"><img src = images/remejai/3.png></a></div>
      <div><a target="_blank" href="http://www.amberqueen.lt/"><img src = images/remejai/4.png></a></div>
      <div><a target="_blank" href="http://www.klaipedosbaldai.lt/"><img src = images/remejai/5.png></a></div>
      <div><a target="_blank" href="http://www.bs-chemical.lt/"><img src = images/remejai/6.jpg></a></div>
      <div><a target="_blank" href="http://www.4corners.lt/"><img src = images/remejai/7.png></a></div>
      <div><a target="_blank" href="http://www.akvile.lt/"><img src = images/remejai/8.png></a></div>
      <div><a target="_blank" href="http://www.monton.lt/"><img src = images/remejai/9.png></a></div>
    </div>
</div>

</section>
<footer>
    <wrapper>
        <nav>
            <ul>
                <li><a href="pages/kasnaujo/kasnaujo.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Naujienos</a></li>
                <li><a href="pages/akademija/akademija.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Akademija</a></li>
                <li><a href="pages/treniruotes/treniruotes.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Treniruotės</a></li>
                <li><a href="pages/tvarkarastis/tvarkarastis.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Pirmenybės</a></li>
                <li><a href="pages/rezultatai/rezultatai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Rezultatai</a></li>
                <li><a href="pages/tvarkarastis/tvarkarastis.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Tvarkaraštis</a></li>
                <li><a href="pages/lenteles/lenteles.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Lentelės</a></li>
                <li><a href="pages/komandos/komandos.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Komandos</a></li>
                <li><a href="pages/nuostatai/nuostatai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Nuostatai</a></li>
                <li><a href="pages/sezonai/sezonai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Praėję sezonai</a></li>
                <li><a href="pages/turnyrai/turnyrai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Turnyrai</a></li>
                <li><a href="pages/amberqueen/amberqueen.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Amber Queen</a></li>
                <li><a href="pages/nuorodos/nuorodos.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Nuorodos</a></li>
                <li><a href="http://www.tinklinioarena.lt"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Paplūdimio tinklinis</a></li>
                <li><a href="pages/kontaktai/kontaktai.php"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Susisiekite</a></li>
                <li><a href="https://www.facebook.com/Klaipedostinklinis/"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Mes Facebook'e</a></li>
            </ul>
        </nav>
        <div class="informacija">
            <div class="rekvizitai">
              <h3>Klaipėdos Tinklinio Asociacija<br></h3>
              <h4>Paramos gavėjo kodas: 302843365<br>Vakario g. 15, Ginduliai, Klaipėdos r.<br>Atsiskaitomoji sąskaita: LT107044060007892226, AB SEB Bankas</h4>
            </div>
            <a href="tel:+37069661004"><i class="fa fa-phone-square" aria-hidden="true"></i>  +370 69 66 1004</a>
            <a href="mailto:info@kta.lt"><i class="fa fa-envelope" aria-hidden="true"></i>  info@kta.lt</a>
        </div>
    </wrapper>
</footer>

    <script src="js/owl.carousel.js"></script>
    <script src="js/kta.js"></script>
</body>
</html>