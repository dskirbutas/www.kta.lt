<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,800,900" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    
</head>
<body>
      <button class="hamburger" style="display: none;">&#9776;</button>
  <div class="meniu"><h2>MENIU</h2></div>
    <header>
        <wrapper>
            <a href="#"><img src="images/logo.png"></a>
            <li><a href="pages/kontaktai/kontaktai.php">Kontaktai</a></li>
            <li><a href="pages/rezultatai/rezultatai.php">Rezultatai</a></li>
            <li><a href="pages/tvarkarastis/tvarkarastis.php">Tvarkaraštis</a></li>
            <a href="#"><li><a href="#">Pradžia</a></li></a>
        </wrapper>
    </header>
<script src="js/jquery.min.js"></script>
<script src="js/hamburger.js"></script>
</body>
</html>